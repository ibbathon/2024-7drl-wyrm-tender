extends Node

signal progress_update(step: String, pcnt_complete: float)
signal cave_generated(map: Map)

enum GenStep {
	CHOOSE_ROOMS,
	DIG_ROOMS,
	CREATE_ASTAR_GRID,
	CONNECT_ROOMS,
	SPAWN_EXIT,
	COMPLETE,
}
var _step_progress = {
	GenStep.CHOOSE_ROOMS: ["Choosing rooms", 0.0],
	GenStep.DIG_ROOMS: ["Digging out rooms", 10.0],
	GenStep.CREATE_ASTAR_GRID: ["Creating nav grid", 30.0],
	GenStep.CONNECT_ROOMS: ["Connecting rooms", 40.0],
	GenStep.SPAWN_EXIT: ["Spawning exit", 95.0],
	GenStep.COMPLETE: ["Complete!", 100.0],
}
var room_gen_map: Dictionary = {
	MapType.RoomType.termites: TermiteRoomGen,
	MapType.RoomType.rectangle: RectangleRoomGen,
}
var path_gen_map: Dictionary = {
	MapType.PathType.arbitrary: ArbitraryPathGen,
	MapType.PathType.manhattan: ManhattanPathGen,
}

var generated_cave_system: Array


## Wrapper for progress_update.emit, as emit cannot be directly deferred.
func _update_progress(step: GenStep, additional_progress: float = 0.0):
	Util.log_process(GenStep.find_key(step) + " " + str(additional_progress), 1)
	var curr_step_text = _step_progress[step][0]
	var curr_step_pcnt = _step_progress[step][1]
	var next_step_pcnt = _step_progress[min(step+1, GenStep.COMPLETE)][1]
	progress_update.emit(
		curr_step_text,
		curr_step_pcnt + (next_step_pcnt - curr_step_pcnt) * additional_progress,
	)


func build_cave_system(map_settings: Map.MapSettings):
	var room_frequency: float = map_settings.room_frequency / 100.0
	var room_size: int = map_settings.room_size
	var cave_size: int = map_settings.cave_size

	var progress_update_timer = get_tree().create_timer(0.1)

	_update_progress.call_deferred(GenStep.CHOOSE_ROOMS)
	var room_locs = []
	# we must have at least two rooms, so keep looping until we generate two
	while len(room_locs) < 2:
		for i in range(cave_size):
			for j in range(cave_size):
				if randf() < room_frequency and Vector2i(j, i) not in room_locs:
					room_locs.append(Vector2i(j, i))

	# create an all-walls or all-floor cave system to start
	var cave_system = Array()
	cave_system.resize(cave_size * room_size)
	for i in range(cave_size * room_size):
		cave_system[i] = Array()
		cave_system[i].resize(cave_size * room_size)
		cave_system[i].fill(map_settings.map_type.fill_terrain)

	_update_progress.call_deferred(GenStep.DIG_ROOMS)
	# now generate the rooms
	var roomgen: RoomGen = room_gen_map[map_settings.map_type.room_type].new(
		room_size,
		map_settings.termite_lifespan,
		map_settings.termite_count,
		map_settings.map_type.fill_terrain,
		map_settings.map_type.room_walls,
	)
	for ri in range(len(room_locs)):
		var loc = room_locs[ri]
		var room = roomgen.build_room()
		for i in range(room_size):
			for j in range(room_size):
				cave_system[j + loc.y * room_size][i + loc.x * room_size] = room[j][i]

		if progress_update_timer.time_left == 0.0:
			_update_progress.call_deferred(GenStep.DIG_ROOMS, float(ri) / len(room_locs))
			progress_update_timer = get_tree().create_timer(0.1)

	# if the fill_terrain is floor, make sure there's a surrounding wall on the very edge
	if map_settings.map_type.fill_terrain == MapEnums.TerrainType.FLOOR:
		var max_idx: int = cave_size * room_size - 1
		for i in range(cave_size * room_size):
			cave_system[i][0] = MapEnums.TerrainType.WALL
			cave_system[0][i] = MapEnums.TerrainType.WALL
			cave_system[i][max_idx] = MapEnums.TerrainType.WALL
			cave_system[max_idx][i] = MapEnums.TerrainType.WALL

	_update_progress.call_deferred(GenStep.CREATE_ASTAR_GRID)
	# put together the A* system we'll use to make sure every room is accessible
	# from every other room
	var navgrid = AStarGrid2D.new()
	navgrid.set_diagonal_mode(AStarGrid2D.DiagonalMode.DIAGONAL_MODE_NEVER)
	navgrid.region = Rect2i(0, 0, cave_size * room_size, cave_size * room_size)
	navgrid.cell_size = Vector2(1, 1)
	navgrid.update()
	for i in range(cave_size * room_size):
		for j in range(cave_size * room_size):
			if cave_system[j][i] == MapEnums.TerrainType.WALL:
				navgrid.set_point_solid(Vector2i(i, j))

	_update_progress.call_deferred(GenStep.CONNECT_ROOMS)
	# now create paths between random rooms until every room is accessible
	var missing_paths: Array = []
	var all_rooms_accessible: bool = false
	var connected_rooms: Array = []

	for i in range(0, len(room_locs) - 1):
		for j in range(i + 1, len(room_locs)):
			missing_paths.append([i, j])
			missing_paths.append([j, i])

	# Build a list of floor tiles in each room.
	# This will be used for choosing random starting/ending locations for each path.
	# This is necessary because not all room-gens (e.g. rectangle) have a floor tile in the center.
	var floor_tiles: Array[Array] = []
	for r in range(0, len(room_locs)):
		var room_loc = room_locs[r]
		var room_rect = Rect2i(room_loc * room_size, Vector2i.ONE * (room_size - 1))
		var room_floors: Array = []
		for j in range(room_rect.position.y, room_rect.end.y + 1):
			for i in range(room_rect.position.x, room_rect.end.x + 1):
				if cave_system[j][i] == MapEnums.TerrainType.FLOOR:
					room_floors.append(Vector2(i, j))
		floor_tiles.append(room_floors)

	var pathgen: PathGen = path_gen_map[map_settings.map_type.path_type].new()
	while not all_rooms_accessible:
		# choose two rooms that we haven't connected before
		var room_idx1 = 0
		var room_idx2 = 0
		var random_path = missing_paths.pick_random()
		room_idx1 = random_path[0]
		room_idx2 = random_path[1]

		# connect them
		var start_loc = floor_tiles[room_idx1].pick_random()
		var end_loc = floor_tiles[room_idx2].pick_random()
		var path = pathgen.build_focused_path(
			Vector2i.ONE,
			# subtract 2 for off-by-1 AND staying away from cave boundary walls
			Vector2i.ONE * (room_size * cave_size - 2),
			start_loc,
			end_loc,
		)
		for loc in path:
			cave_system[loc.y][loc.x] = MapEnums.TerrainType.FLOOR
			navgrid.set_point_solid(loc, false)
		connected_rooms.append([room_idx1, room_idx2])
		connected_rooms.append([room_idx2, room_idx1])

		# now check if we've successfully connected all rooms
		missing_paths = []
		var total_connections = len(room_locs) * (len(room_locs) - 1) / 2
		var connections_made = 0
		all_rooms_accessible = true

		for i in range(0, len(room_locs) - 1):
			for j in range(i + 1, len(room_locs)):
				var navpath = navgrid.get_id_path(
					floor_tiles[i].pick_random(),
					floor_tiles[j].pick_random(),
				)
				if navpath.is_empty():
					missing_paths.append([i, j])
					missing_paths.append([j, i])
					all_rooms_accessible = false
				else:
					connections_made += 1
		if progress_update_timer.time_left == 0.0:
			_update_progress.call_deferred(
				GenStep.CONNECT_ROOMS,
				float(connections_made) / total_connections,
			)
			progress_update_timer = get_tree().create_timer(0.1)

	_update_progress.call_deferred(GenStep.SPAWN_EXIT)
	# Finally, spawn an exit tile at a random location on the edge, then build a path away from
	# that edge to a floor tile.
	var exit_loc: Vector2i = Vector2i.ZERO
	while exit_loc == Vector2i.ZERO:
		var max_index = len(cave_system[0])-1
		var max_dist = len(cave_system)-1
		# default to left edge
		var horiz_edge = false
		var zero_edge = true
		# randomly switch to top edge
		if randf() > 0.5:
			max_index = len(cave_system)-1
			max_dist = len(cave_system[0])-1
			horiz_edge = true
		# randomly switch to right/bottom edge
		if randf() > 0.5:
			zero_edge = false

		# Don't spawn on the very top or bottom, as that will cause issues.
		var spawn_dist = randi_range(2, max_dist - 2)
		exit_loc = Vector2i(
			spawn_dist if horiz_edge else (0 if zero_edge else max_index),
			spawn_dist if not horiz_edge else (0 if zero_edge else max_index),
		)

		var increment = 1 if zero_edge else -1
		var floor_dist = increment + (exit_loc.y if horiz_edge else exit_loc.x)
		while (
			floor_dist > 0 and floor_dist < max_index and (
				(
					horiz_edge and
					cave_system[floor_dist][exit_loc.x] != MapEnums.TerrainType.FLOOR
				) or (
					not horiz_edge and
					cave_system[exit_loc.y][floor_dist] != MapEnums.TerrainType.FLOOR
				)
			)
		):
			floor_dist += increment
		# If we failed to path to a floor tile, restart.
		if floor_dist == 0 or floor_dist == max_index:
			exit_loc = Vector2i.ZERO
			continue

		# Carve the path to that floor tile.
		while floor_dist > 0 and floor_dist < max_index:
			if horiz_edge:
				cave_system[floor_dist][exit_loc.x] = MapEnums.TerrainType.FLOOR
				navgrid.set_point_solid(Vector2i(exit_loc.x, floor_dist), false)
			else:
				cave_system[exit_loc.y][floor_dist] = MapEnums.TerrainType.FLOOR
				navgrid.set_point_solid(Vector2i(floor_dist, exit_loc.y), false)
			floor_dist -= increment

		# Finally, we need to check that the exit is actually accessible. This is due to the
		# town maptype (and other default-to-floor maptypes) possibly generating room walls in
		# such a way that the exit_loc is next to a floor, but said floor is inaccessible to the
		# play area. This would be extremely rare, but still possible.
		var start_loc = floor_tiles.pick_random().pick_random()
		navgrid.set_point_solid(exit_loc, false)
		var navpath = navgrid.get_id_path(start_loc, exit_loc)
		if navpath.is_empty():
			navgrid.set_point_solid(exit_loc, true)
			exit_loc = Vector2i.ZERO
			continue

		# Everything is connected, so set the exit and we're done!
		cave_system[exit_loc.y][exit_loc.x] = MapEnums.TerrainType.EXIT


	_update_progress.call_deferred(GenStep.COMPLETE)
	# Now we re-interpret the rooms and paths into a more useable form, then stuff
	# all of it into the Map object for sending back to the main thread.
	var map: Map = Map.new()
	map.map_settings = map_settings
	map.terrain = cave_system
	map.exit_loc = exit_loc
	map.rooms = []
	map.rooms.resize(len(room_locs))
	map.room_floors = floor_tiles
	map.paths = []
	map.paths.resize(len(room_locs))
	for i in range(len(room_locs)):
		var room_loc = room_locs[i]
		map.rooms[i] = Rect2i(room_loc * room_size, Vector2i.ONE * (room_size - 1))
		map.paths[i] = []
	for connection in connected_rooms:
		map.paths[connection[0]].append(connection[1])

	(func(): cave_generated.emit(map)).call_deferred()
