extends PathGen
class_name ArbitraryPathGen


func build_focused_path(
	min_bounds: Vector2i, max_bounds: Vector2i, start_loc: Vector2i, end_loc: Vector2i,
) -> PackedVector2Array:
	var termite = Termite.new(min_bounds, max_bounds, start_loc)
	# prevent an infinite loop by only calling the step function a limited number of times
	for _counter in range(20):
		var direction: Vector2 = Vector2(end_loc) - termite._locations[-1]
		var optimistic_step_count = abs(direction.x) + abs(direction.y)
		direction = direction.normalized() * 10
		var weights = Vector4.ONE * 0.1
		if direction.x > 0:
			weights.y = direction.x
		else:
			weights.w = -direction.x
		if direction.y > 0:
			weights.z = direction.y
		else:
			weights.x = -direction.y

		termite.update_weights(weights)
		termite.step(optimistic_step_count)
		if Vector2(end_loc) in termite._locations:
			break

	return termite._locations
