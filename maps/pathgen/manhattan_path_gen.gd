extends PathGen
class_name ManhattanPathGen


func build_focused_path(
	_min_bounds: Vector2i, _max_bounds: Vector2i, start_loc: Vector2i, end_loc: Vector2i,
) -> PackedVector2Array:
	var tiles: PackedVector2Array = [start_loc]
	if randf() < 0.5:
		_traverse_x(tiles, end_loc)
		_traverse_y(tiles, end_loc)
	else:
		_traverse_y(tiles, end_loc)
		_traverse_x(tiles, end_loc)
	return tiles


func _traverse_x(tiles: PackedVector2Array, end_loc: Vector2i) -> void:
	var last_tile: Vector2 = tiles[-1]
	var increment: int = 1
	if last_tile.x > end_loc.x:
		increment = -1
	for i in range(int(tiles[-1].x) + increment, end_loc.x, increment):
		last_tile.x = i
		tiles.append(last_tile)
	last_tile.x = end_loc.x
	tiles.append(last_tile)


func _traverse_y(tiles: PackedVector2Array, end_loc: Vector2i) -> void:
	var last_tile: Vector2 = tiles[-1]
	var increment: int = 1
	if last_tile.y > end_loc.y:
		increment = -1
	for i in range(int(tiles[-1].y) + increment, end_loc.y, increment):
		last_tile.y = i
		tiles.append(last_tile)
	last_tile.y = end_loc.y
	tiles.append(last_tile)
