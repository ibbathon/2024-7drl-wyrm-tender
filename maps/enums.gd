extends Node
class_name MapEnums

enum TerrainType {
	WALL,
	FLOOR,
	EXIT,
}

enum LevelModifier {
	raging_ai,
}

enum GoalType {
	kill_mob,
	loot,
	exit,
}
