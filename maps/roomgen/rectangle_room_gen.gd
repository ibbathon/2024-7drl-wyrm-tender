extends RoomGen
class_name RectangleRoomGen

## Builds a rectangular room by randomly determining the size (less than room_size) and location.
## Note that the room size includes the enclosing walls.
func build_room() -> Array:
	var max_size = Vector2i.ONE * room_size
	# must be a minimum of 3 to allow a floor tile surrounded by walls
	# TODO: this min-size thing should be handled higher up
	max_size.x = max(3, max_size.x)
	max_size.y = max(3, max_size.y)

	var room = []
	room.resize(max_size.y)
	for i in range(max_size.y):
		room[i] = []
		room[i].resize(max_size.x)
		room[i].fill(fill_terrain)

	var min_rect_size = Vector2i((len(room[0]) - 3) / 2, (len(room) - 3) / 2)
	var rect = Rect2i(
		1,
		1,
		randi_range(min_rect_size.x, len(room[0]) - 3),
		randi_range(min_rect_size.y, len(room) - 3),
	)
	rect.position.x = randi_range(1, len(room[0]) - 2 - rect.size.x)
	rect.position.y = randi_range(1, len(room) - 2 - rect.size.y)

	for i in range(rect.position.x, rect.end.x + 1):
		for j in range(rect.position.y, rect.end.y + 1):
			room[j][i] = MapEnums.TerrainType.FLOOR

	if room_walls == MapType.RoomWalls.forced:
		for i in range(rect.position.x - 1, rect.end.x + 2):
			room[rect.position.y - 1][i] = MapEnums.TerrainType.WALL
			room[rect.end.y + 1][i] = MapEnums.TerrainType.WALL
		for j in range(rect.position.y - 1, rect.end.y + 2):
			room[j][rect.position.x - 1] = MapEnums.TerrainType.WALL
			room[j][rect.end.x + 1] = MapEnums.TerrainType.WALL

	return room
