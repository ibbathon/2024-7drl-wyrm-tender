extends RoomGen
class_name TermiteRoomGen

## Procedurally generates a room by filling it with walls and then setting loose
## "termites" to chew through the walls randomly.
## Note that the room size includes the enclosing walls.
func build_room() -> Array:
	var max_size = Vector2i.ONE * room_size
	# subtract 1 to account for index-off-by-1
	# must be a minimum of 2 to allow a floor tile surrounded by walls
	max_size.x = max(2, max_size.x - 1)
	max_size.y = max(2, max_size.y - 1)

	var termite_paths = []
	for i in range(termite_count):
		var start = max_size / 2
		# create a termite that won't go outside a 1-thick boundary around the room
		var termite = Termite.new(Vector2i.ONE, max_size - Vector2i.ONE, start)
		termite.step(termite_lifespan)
		termite_paths.append(termite._locations)

	# re-adjust max_size back to include the off-by-1
	max_size += Vector2i.ONE

	var room = []
	room.resize(max_size.y)
	for i in range(max_size.y):
		room[i] = []
		room[i].resize(max_size.x)
		room[i].fill(MapEnums.TerrainType.WALL)

	# now actually have the termites eat through the walls
	for path in termite_paths:
		for pos in path:
			room[pos.y][pos.x] = MapEnums.TerrainType.FLOOR

	return room
