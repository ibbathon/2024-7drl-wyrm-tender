extends Node
class_name RoomGen

var room_size: int
var termite_lifespan: int
var termite_count: int
var fill_terrain: MapEnums.TerrainType
var room_walls: MapType.RoomWalls


func _init(
	room_size_: int,
	termite_lifespan_: int,
	termite_count_: int,
	fill_terrain_: MapEnums.TerrainType,
	room_walls_: MapType.RoomWalls,
) -> void:
	room_size = room_size_
	termite_lifespan = termite_lifespan_
	termite_count = termite_count_
	fill_terrain = fill_terrain_
	room_walls = room_walls_


func build_room() -> Array:
	return []
