extends Node
class_name Map


class MapSettings:
	## The maptype file to use.
	var map_type: MapType

	## Frequency to select a cave cell as a room; 0-100
	var room_frequency: int

	## Size of each room
	var room_size: int

	## Size of the cave, measured in room units.
	## e.g. if cave_size = 5 and room_size = 10, then the generated map will be 50x50 tiles
	var cave_size: int

	## Number of termites to spawn in each room.
	var termite_count: int

	## How long each termite should keep digging.
	var termite_lifespan: int

	func _init(
		map_type_: String,
		room_frequency_: int,
		room_size_: int,
		cave_size_: int,
		termite_count_: int,
		termite_lifespan_: int,
	):
		self.map_type = null if map_type_ == "null" else Data.map_types[map_type_]
		self.room_frequency = room_frequency_
		self.room_size = room_size_
		self.cave_size = cave_size_
		self.termite_count = termite_count_
		self.termite_lifespan = termite_lifespan_

	func _to_string() -> String:
		return str({
			"map_type": map_type,
			"room_frequency": room_frequency,
			"room_size": room_size,
			"cave_size": cave_size,
			"termite_count": termite_count,
			"termite_lifespan": termite_lifespan,
		})


## 2D array of TerrainType
var terrain: Array

## The sole exit location
var exit_loc: Vector2i

## Array of Rect2i corresponding to the rooms generated
var rooms: Array

## Array of arrays of Vector2s representing the generated floorspace of each room.
var room_floors: Array

## Array of arrays of indexes into the rooms array, corresponding to the paths
## generated between rooms. e.g. paths[2] == [3, 5] means that room 2 is connected to
## rooms 3 and 5, where room x is defined by the tiles in the rect rooms[x]
var paths: Array

## Map settings used to generate this map
var map_settings: MapSettings
