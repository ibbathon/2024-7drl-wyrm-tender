extends Node
class_name MapType


enum RoomType {
	termites,
	rectangle,
}

enum RoomWalls {
	optional,
	forced,
}

enum PathType {
	arbitrary,
	manhattan,
}

enum FillTile {
	floor,
	wall,
}

var fill_tile_to_terrain_type: Dictionary = {
	FillTile.floor: MapEnums.TerrainType.FLOOR,
	FillTile.wall: MapEnums.TerrainType.WALL,
}

var filename: String
var texture: CompressedTexture2D
var room_type: RoomType
var room_walls: RoomWalls
var path_type: PathType
var fill_tile: FillTile
var fill_terrain: MapEnums.TerrainType
var max_room_frequency: bool = false


func _init(filename_: String, json_data: Dictionary):
	filename = filename_
	var errored = false
	if "texture" in json_data:
		texture = load(json_data["texture"])
		if texture == null:
			Util.log_error("Map type " + filename + ": invalid texture file")
			errored = true

	if "max_room_frequency" in json_data:
		max_room_frequency = bool(json_data["max_room_frequency"])

	for enum_param in [
		["room_type", RoomType],
		["room_walls", RoomWalls],
		["path_type", PathType],
		["fill_tile", FillTile],
	]:
		var enumname: String = enum_param[0]
		var enumtype: Dictionary = enum_param[1]
		if enumname in json_data:
			var enum_val = enumtype.get(json_data[enumname])
			if enum_val == null:
				Util.log_error("Map type " + filename + ": invalid " + enumname)
				errored = true
			else:
				set(enumname, enum_val)

	fill_terrain = fill_tile_to_terrain_type[fill_tile]

	if errored:
		Util.log_error("Failed to interpret map type: " + filename_ + "; " + str(json_data))
		Util.get_tree().quit()


func _to_string() -> String:
	return "MapType " + filename
