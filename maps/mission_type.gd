extends Node
class_name MissionType


class TypeLists:
	## whitelist overrides blacklisting
	var whitelist: Array[String]
	var blacklist: Array[String]

	func _init(whitelist_: Array[String], blacklist_: Array[String]):
		whitelist = whitelist_
		blacklist = blacklist_

class Goal:
	var goal_type: MapEnums.GoalType
	var quantity_limits: Array[int]
	var score: int
	var frequency: int

	func _init(
		goal_type_: MapEnums.GoalType, quantity_limits_: Array[int], score_: int, frequency_: int = 100
	):
		goal_type = goal_type_
		quantity_limits = quantity_limits_
		score = score_
		frequency = frequency_


	func _to_string() -> String:
		return MapEnums.GoalType.find_key(goal_type)

class Stage:
	var instructions: String
	var goals: Array[Goal]

	func _init(instructions_: String, goals_: Array[Goal]):
		instructions = instructions_
		goals = goals_


var filename: String
var modifiers: Array[MapEnums.LevelModifier]
var mission_text: String
var mission_frequency: int
var cave_size_limits: Array[int]
var room_size_limits: Array[int]
var room_freq_limits: Array[int]
var mob_frequencies: Array[int]
var gold_frequencies: Array[int]
var mob_types_lists: TypeLists
var map_types_lists: TypeLists
var valid_mob_types: Array[MobType]
var valid_map_types: Array[MapType]
var stages: Array[Stage]
var possible_optional_goals: Array[Goal]


func _init(filename_: String, json_data: Dictionary):
	filename = filename_
	mission_text = json_data["mission_text"]
	mission_frequency = json_data.get("mission_frequency_override", 100)
	modifiers = []
	for modstr in json_data.get("modifiers", []):
		modifiers.append(MapEnums.LevelModifier.get(modstr))
	for key in [
		"cave_size_limits",
		"room_size_limits",
		"room_freq_limits",
		"mob_frequencies",
		"gold_frequencies",
	]:
		var list: Array[int]
		# This .assign nonsense is necessary due to how typing works in Godot.
		list.assign(json_data[key])
		set(key, list)
	for key in [
		"mob_types_lists",
		"map_types_lists",
	]:
		var whitelist: Array[String]
		var blacklist: Array[String]
		whitelist.assign(json_data[key].get("whitelist", []))
		blacklist.assign(json_data[key].get("blacklist", []))
		set(key, TypeLists.new(whitelist, blacklist))

	json_data.get_or_add("possible_optional_goals", [])
	for goal_dict in json_data["possible_optional_goals"]:
		var quantity: Array[int]
		quantity.assign(goal_dict.get("quantity", [0, 0]))
		var goal = Goal.new(
			MapEnums.GoalType.get(goal_dict["goal_type"]),
			quantity,
			goal_dict.get("score", 0),
			goal_dict.get("frequency", 100),
		)
		possible_optional_goals.append(goal)

	stages = []
	for stage_dict in json_data["stages"]:
		var goals: Array[Goal] = []
		for goal_dict in stage_dict["goals"]:
			var quantity: Array[int]
			quantity.assign(goal_dict.get("quantity", [0, 0]))
			var goal = Goal.new(
				MapEnums.GoalType.get(goal_dict["goal_type"]),
				quantity,
				goal_dict.get("score", 0),
				goal_dict.get("frequency", 100),
			)
			goals.append(goal)
		var stage = Stage.new(stage_dict["instructions"], goals)
		stages.append(stage)


func _determine_valid_types():
	valid_map_types = []
	valid_mob_types = []
	for keys in [
		[valid_map_types, map_types_lists, Data.map_types],
		[valid_mob_types, mob_types_lists, Data.mob_types],
	]:
		var valid_types = keys[0]
		var types_lists = keys[1]
		var all_types = keys[2]
		for type_ in all_types:
			if (
				type_ in types_lists.whitelist or
				type_ not in types_lists.blacklist
			):
				valid_types.append(all_types[type_])


func generate_mission():
	if valid_map_types == []:
		_determine_valid_types()

	var map_type: MapType = valid_map_types.pick_random()
	Util.log_rng("map_type: %s" % map_type, 1)
	var mob_types: Array[MobType] = []
	while mob_types.is_empty():
		for mob_type in valid_mob_types:
			if randf() > 0.5:
				mob_types.append(mob_type)
	Util.log_rng("mob_types: " + str(mob_types), 1)

	var cave_size: int = randi_range(cave_size_limits[0], cave_size_limits[1])
	var room_size: int = randi_range(room_size_limits[0], room_size_limits[1])
	var room_freq: int = randi_range(room_freq_limits[0], room_freq_limits[1])
	if map_type.max_room_frequency:
		room_freq = room_freq_limits[1]
	Util.log_rng(
		"cave_size, room_size, room_freq: %s, %s, %s" % [cave_size, room_size, room_freq], 1
	)

	# Calculate a limiter to prevent missions from requiring too many kills/pickups.
	var actor_limiter: int = int(
		cave_size * cave_size # max num of rooms
		* float(room_freq) / 100 # adjust by room frequency
		* 3 # max of 3 mobs per room
	)
	actor_limiter = max(1, actor_limiter)
	Util.log_rng("actor_limiter: %s" % actor_limiter, 1)

	var optional_goals: Array[Mission.Goal] = []
	for goal_type in possible_optional_goals:
		Util.log_process("processing optional goal: %s" % goal_type, 1)
		if randi_range(0, 100) > goal_type.frequency:
			Util.log_rng("not adding optional goal", 1)
			continue
		Util.log_rng("adding optional goal", 1)
		var goal_quantity = randi_range(goal_type.quantity_limits[0], goal_type.quantity_limits[1])
		Util.log_rng("rand goal quant: %s" % goal_quantity, 1)
		goal_quantity = min(actor_limiter, goal_quantity)
		Util.log_rng("after limiting: %s" % goal_quantity, 1)
		var goal = Mission.Goal.new(
			goal_type.goal_type,
			min(actor_limiter, goal_quantity),
			goal_type.score,
		)
		optional_goals.append(goal)

	var stage_instances: Array[Mission.Stage] = []
	for stage_type in stages:
		var goal_instances: Array[Mission.Goal] = []
		for goal_type in stage_type.goals:
			Util.log_process("processing goal: %s" % goal_type, 1)
			var goal_quantity = randi_range(
				goal_type.quantity_limits[0], goal_type.quantity_limits[1]
			)
			Util.log_rng("rand goal quant: %s" % goal_quantity, 1)
			goal_quantity = min(actor_limiter, goal_quantity)
			Util.log_rng("after limiting: %s" % goal_quantity, 1)
			var goal = Mission.Goal.new(
				goal_type.goal_type,
				goal_quantity,
				goal_type.score,
			)
			goal_instances.append(goal)
		var stage = Mission.Stage.new(stage_type.instructions, goal_instances)
		stage_instances.append(stage)

	return Mission.new(
		modifiers,
		mission_text,
		cave_size,
		room_size,
		room_freq,
		mob_frequencies,
		gold_frequencies,
		mob_types,
		map_type,
		optional_goals,
		stage_instances,
	)
