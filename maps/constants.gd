extends Node
class_name MapConstants

const TILE_VARIANT_FREQUENCY: int = 5
const TILE_VARIANT_AMOUNT: int = 10
const TILESET_SOURCE: int = 1
const TILESET_FLOOR_DEFAULT: Vector2i = Vector2i(0, 0)
const TILESET_WALL_DEFAULT: Vector2i = Vector2i(0, 1)
const TILESET_FLOOR_VARIANTS: Array[Vector2i] = [
	Vector2i(1, 0), Vector2i(2, 0), Vector2i(3, 0), Vector2i(4, 0), Vector2i(5, 0),
	Vector2i(6, 0), Vector2i(7, 0), Vector2i(8, 0), Vector2i(9, 0),
]
const TILESET_WALL_VARIANTS: Array[Vector2i] = [
	Vector2i(1, 1), Vector2i(2, 1), Vector2i(3, 1), Vector2i(4, 1), Vector2i(5, 1),
	Vector2i(6, 1), Vector2i(7, 1), Vector2i(8, 1), Vector2i(9, 1),
]

static var terrain_to_tileset_default: Dictionary = {
	MapEnums.TerrainType.WALL: TILESET_WALL_DEFAULT,
	MapEnums.TerrainType.FLOOR: TILESET_FLOOR_DEFAULT,
	MapEnums.TerrainType.EXIT: TILESET_FLOOR_DEFAULT,
}
static var terrain_to_tileset_variants: Dictionary = {
	MapEnums.TerrainType.WALL: TILESET_WALL_VARIANTS,
	MapEnums.TerrainType.FLOOR: TILESET_FLOOR_VARIANTS,
	MapEnums.TerrainType.EXIT: [TILESET_FLOOR_DEFAULT] as Array[Vector2i],
}
