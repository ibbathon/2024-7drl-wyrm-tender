## A random walker used to "tunnel" through walls to create rooms/paths.
extends Node
class_name Termite

## A vector array of locations this termite has visited.
var _locations: PackedVector2Array

## Stored version of the map bounds.
var _min_bounds: Vector2i
var _max_bounds: Vector2i

## The weights to assign to each direction: up, right, down, left.
var _move_weights: Vector4

## Helper so we don't have to recalculate the weight sums on every step.
var _total_weights: Vector4

func _init(min_bounds: Vector2i, max_bounds: Vector2i, start_loc: Vector2i, move_weights: Vector4 = Vector4.ONE):
	_locations = [start_loc]
	update_weights(move_weights)
	_min_bounds = min_bounds
	_max_bounds = max_bounds

func step(count: int = 1):
	for i in range(count):
		# choose a direction
		var dir_rand = randf_range(0, _total_weights.w)
		var direction = Vector2i.ZERO
		if dir_rand < _total_weights.x:
			direction.y = -1
		elif dir_rand < _total_weights.y:
			direction.x = 1
		elif dir_rand < _total_weights.z:
			direction.y = 1
		else:
			direction.x = -1

		var new_point = _locations[-1] + Vector2(direction)
		new_point = new_point.clamp(_min_bounds, _max_bounds)

		_locations.append(new_point)

func update_weights(new_weights: Vector4):
	_move_weights = new_weights
	_total_weights.x = new_weights.x
	_total_weights.y = _total_weights.x + new_weights.y
	_total_weights.z = _total_weights.y + new_weights.z
	_total_weights.w = _total_weights.z + new_weights.w
