extends Node
class_name Mission

signal goal_complete(score: int)
signal stage_complete(is_last: bool)

enum ActivityType {
	mob_killed,
	gold_looted,
	exit_reached,
}

class Goal:
	signal progressed(progress: int, out_of: int)
	var goal_type: MapEnums.GoalType
	var quantity: int
	var _progress: int
	var progress: int:
		set(value):
			_progress = value
			progressed.emit(_progress, quantity)
		get():
			return _progress
	var complete: bool
	var score: int

	func _init(goal_type_: MapEnums.GoalType, quantity_: int, score_: int):
		goal_type = goal_type_
		quantity = quantity_
		score = score_
		_progress = 0
		complete = false

class Stage:
	var instructions: String
	var goals: Array[Goal]

	func _init(instructions_: String, goals_: Array[Goal]):
		instructions = instructions_
		goals = goals_


var modifiers: Array[MapEnums.LevelModifier]
var mission_text: String
var cave_size: int
var room_size: int
var room_freq: int
var mob_frequencies: Array[int]
var gold_frequencies: Array[int]
var min_mob_count: int
var min_gold_count: int
var mob_types: Array[MobType]
var map_type: MapType
var optional_goals: Array[Goal]
var stages: Array[Stage]
var active_stage_idx: int


func _init(
	modifiers_: Array[MapEnums.LevelModifier],
	mission_text_: String,
	cave_size_: int,
	room_size_: int,
	room_freq_: int,
	mob_frequencies_: Array[int],
	gold_frequencies_: Array[int],
	mob_types_: Array[MobType],
	map_type_: MapType,
	optional_goals_: Array[Goal],
	stages_: Array[Stage],
):
	modifiers = modifiers_
	mission_text = mission_text_
	cave_size = cave_size_
	room_size = room_size_
	room_freq = room_freq_
	mob_frequencies = mob_frequencies_
	gold_frequencies = gold_frequencies_
	mob_types = mob_types_
	map_type = map_type_
	optional_goals = optional_goals_
	stages = stages_
	active_stage_idx = 0

	min_mob_count = 0
	min_gold_count = 0
	for stage in stages:
		for goal in stage.goals:
			if goal.goal_type == MapEnums.GoalType.kill_mob:
				min_mob_count += goal.quantity
			if goal.goal_type == MapEnums.GoalType.loot:
				min_gold_count += goal.quantity
	for goal in optional_goals:
		if goal.goal_type == MapEnums.GoalType.kill_mob:
			min_mob_count += goal.quantity
		if goal.goal_type == MapEnums.GoalType.loot:
			min_gold_count += goal.quantity


func get_current_goals() -> Array[Goal]:
	var active_goals: Array[Goal] = []
	for goal in stages[active_stage_idx].goals:
		if not goal.complete:
			active_goals.append(goal)
	return active_goals


func get_optional_goals() -> Array[Goal]:
	var active_goals: Array[Goal] = []
	for goal in optional_goals:
		if not goal.complete:
			active_goals.append(goal)
	return active_goals


func _goal_matches_activity(goal: Goal, activity: ActivityType) -> bool:
	if (
		goal.goal_type == MapEnums.GoalType.kill_mob and
		activity == ActivityType.mob_killed
	):
		return true
	elif (
		goal.goal_type == MapEnums.GoalType.loot and
		activity == ActivityType.gold_looted
	):
		return true
	elif (
		goal.goal_type == MapEnums.GoalType.exit and
		activity == ActivityType.exit_reached
	):
		return true

	return false


func update_progress(activity_type):
	var progress_made = false
	for goal_set in [stages[active_stage_idx].goals, optional_goals]:
		for goal in goal_set:
			if goal.complete:
				continue

			if _goal_matches_activity(goal, activity_type):
				progress_made = true
				goal.progress += 1
				if goal.progress >= goal.quantity:
					goal.complete = true
					goal_complete.emit(goal.score)

	if progress_made:
		var incomplete_goal: bool = false
		for goal in stages[active_stage_idx].goals:
			if not goal.complete:
				incomplete_goal = true
				break
		if not incomplete_goal:
			if active_stage_idx == len(stages) - 1:
				stage_complete.emit(true)
			else:
				active_stage_idx += 1
				stage_complete.emit(false)
