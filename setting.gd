extends Node

enum SettingType {
	BOOL,
	PCNT,
	SLIDER,
	TEXT,
	DROPDOWN,
	HIDDEN,
}


class Subsettings:
	static func copy(_subsettings_: Subsettings) -> Subsettings:
		return Subsettings.new()


class SliderSubsettings:
	extends Subsettings

	var minval: float
	var maxval: float
	var step: float

	func _init(minval_: float, maxval_: float, step_: float):
		minval = minval_
		maxval = maxval_
		step = step_

	static func copy(subsettings_: Subsettings) -> Subsettings:
		return SliderSubsettings.new(subsettings_.minval, subsettings_.maxval, subsettings_.step)


class DropdownSubsettings:
	extends Subsettings

	## value_map is a string-to-string map where keys are the human-readable, while values are
	## the value that the game will use.
	var value_map: Dictionary

	func _init(value_map_: Dictionary):
		value_map = value_map_

	static func copy(subsettings_: Subsettings) -> Subsettings:
		return DropdownSubsettings.new(subsettings_.value_map)


var type: SettingType
var desc: String
var value
var subsettings: Subsettings
var visibility_grouping: int
var affects_group: int

func _init(
	type_: SettingType, name_: String, desc_: String, value_, subsettings_: Subsettings = null,
	visibility_grouping_: int = -1, affects_group_: int = -1,
):
	type = type_
	name = name_
	desc = desc_
	value = value_
	subsettings = subsettings_
	visibility_grouping = visibility_grouping_
	affects_group = affects_group_

static func copy(setting_):
	return new(
		setting_.type, setting_.name, setting_.desc, setting_.value,
		Subsettings.copy(setting_.subsettings),
		setting_.visibility_grouping, setting_.affects_group,
	)
