@tool
extends HBoxContainer

signal toggled(toggled_on: bool)

@export var label_text: String:
	get:
		return $Label.text
	set(value):
		$Label.text = value

@export var default_value: bool:
	get:
		return $CheckButton.button_pressed
	set(value):
		$CheckButton.button_pressed = value


func _on_check_button_toggled(toggled_on: bool) -> void:
	toggled.emit(toggled_on)
