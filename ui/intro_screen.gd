extends Control

signal continue_game


func _input(event: InputEvent) -> void:
	if not visible:
		return
	if event.is_action_released("ui_accept"):
		get_viewport().set_input_as_handled()
		continue_game.emit()


func _on_continue_pressed():
	continue_game.emit()


func update_name(new_name: String):
	$YouAreKobold.text = "You are " + new_name + ", kobold!"


func update_intro_text(intro_text: String, dragon_name: String, mob_type: String):
	$VBoxContainer/IntroText.text = intro_text.format(
		{"dragon_name": dragon_name, "mob_type": mob_type}
	)
