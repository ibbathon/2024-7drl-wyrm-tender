@tool
extends HBoxContainer

@export var label_text: String:
	set(value):
		$Label.text = value
	get():
		return $Label.text

func update_progress(cur_val: int, max_val: int) -> void:
	$ProgressBar/CurAndMaxValue.text = "%s/%s" % [cur_val, max_val]
	$ProgressBar.value = float(cur_val) / max_val
