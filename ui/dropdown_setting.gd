@tool
extends HBoxContainer

var _value_map: Dictionary = {}
var _value: String

@export var label_text: String:
	get:
		return $Label.text
	set(value):
		$Label.text = value

@export var value_map: Dictionary:
	get:
		return _value_map
	set(value_map_):
		_value_map = value_map_

@export var default_value: String:
	get:
		if $OptionButton.selected == -1:
			return ""
		else:
			return _value_map[$OptionButton.get_item_text($OptionButton.get_selected_id())]
	set(value):
		_value = value


func _ready():
	for key in _value_map:
		$OptionButton.add_item(key)
	if _value != "":
		var key = _value_map.find_key(_value)
		var idx = _value_map.keys().find(key)
		$OptionButton.select(idx)
