extends TabContainer

var _controls: Dictionary
var _visibility_groups: Dictionary
var ToggleSetting = preload("res://ui/toggle_setting.tscn")
var SliderSetting = preload("res://ui/slider_setting.tscn")
var TextSetting = preload("res://ui/text_setting.tscn")
var DropdownSetting = preload("res://ui/dropdown_setting.tscn")
var Setting = preload("res://setting.gd")
var SettingType = Setting.SettingType


func _ready() -> void:
	# Put together a simple sample UI if running with F6.
	if get_parent() == get_tree().root:
		_sample_settings()


func _sample_settings() -> void:
	populate_from_settings({
		"First Tab": [
			Setting.new(
				SettingType.BOOL, "a_val", "A Value", true, null, -1, 0
			),
			Setting.new(
				SettingType.TEXT, "some_name", "Some Name", "Fred", null, 0
			),
			Setting.new(
				SettingType.DROPDOWN, "a_dropdown", "A Dropdown", "second_option",
				Setting.DropdownSubsettings.new({
					"First Option": "first_option",
					"Second Option": "second_option",
				}),
			),
		],
	})
	print(get_value("First Tab", "a_val"))
	print(get_all_values())


## Builds a tabbed settings interface from a given list of settings.
## settings should be a dictionary of lists, with the keys being the tab names/sections
## and the lists containing Setting objects. Other methods will use these "name" attribute
## of those Setting objects to get/set settings.
func populate_from_settings(settings: Dictionary):
	# clear old settings first
	for child in get_children():
		child.queue_free()
	_controls = {}
	var _group_controls = {}
	for tab_name in settings:
		_controls[tab_name] = {}
		var scroll = ScrollContainer.new()
		add_child(scroll)
		scroll.name = tab_name
		var style = StyleBoxEmpty.new()
		style.content_margin_top = 10
		scroll.add_theme_stylebox_override("panel", style)
		var vbox = VBoxContainer.new()
		scroll.add_child(vbox)
		vbox.alignment = BoxContainer.ALIGNMENT_BEGIN
		vbox.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		vbox.size_flags_vertical = Control.SIZE_EXPAND_FILL
		var tab_settings = settings[tab_name]
		for setting in tab_settings:
			var control
			if setting.type == SettingType.HIDDEN:
				continue
			elif setting.type == SettingType.BOOL:
				control = ToggleSetting.instantiate()
			elif setting.type == SettingType.TEXT:
				control = TextSetting.instantiate()
			elif setting.type == SettingType.PCNT:
				control = SliderSetting.instantiate()
				control.min_value = 0
				control.max_value = 100
				control.step_size = 1
			elif (
				setting.type == SettingType.SLIDER and
				is_instance_of(setting.subsettings, Setting.SliderSubsettings)
			):
				control = SliderSetting.instantiate()
				control.min_value = setting.subsettings.minval
				control.max_value = setting.subsettings.maxval
				control.step_size = setting.subsettings.step
			elif (
				setting.type == SettingType.DROPDOWN and
				is_instance_of(setting.subsettings, Setting.DropdownSubsettings)
			):
				control = DropdownSetting.instantiate()
				control.value_map = setting.subsettings.value_map
			else:
				var error = "Failed to parse setting type: " + str(setting.type) + " " + setting.name
				Util.log_error(error)
				continue

			control.name = setting.name
			control.label_text = setting.desc
			control.default_value = setting.value
			vbox.add_child(control)
			_controls[tab_name][setting.name] = control
			if setting.visibility_grouping >= 0:
				_visibility_groups.get_or_add(setting.visibility_grouping, []).append(control)
			# TODO: we only support toggling visibility groups with toggle settings for now
			if setting.affects_group >= 0 and setting.type == SettingType.BOOL:
				control.toggled.connect(_toggle_group_visibility.bind(setting.affects_group))
				_group_controls[setting.affects_group] = control.default_value

	for group in _group_controls:
		_toggle_group_visibility(_group_controls[group], group)


func get_value(tab_name, setting_name):
	return _controls[tab_name][setting_name].default_value


func get_all_values() -> Dictionary:
	var values = {}
	for tab_name in _controls:
		values[tab_name] = {}
		for setting_name in _controls[tab_name]:
			values[tab_name][setting_name] = _controls[tab_name][setting_name].default_value
	return values


func load_all_values(new_settings):
	for tab_name in new_settings:
		if tab_name not in _controls:
			continue
		for setting_name in new_settings[tab_name]:
			if setting_name not in _controls[tab_name]:
				continue
			var value = new_settings[tab_name][setting_name]
			if is_instance_of(value, Setting):
				value = value.value
			_controls[tab_name][setting_name].default_value = value


## Returns true if any of the text boxes in this menu have focus.
func is_text_box_focused() -> bool:
	for box in find_children("*", "LineEdit", true, false):
		if box.has_focus():
			return true
	return false


func _toggle_group_visibility(toggled_on: bool, visibility_group: int):
	for control in _visibility_groups[visibility_group]:
		control.visible = toggled_on
