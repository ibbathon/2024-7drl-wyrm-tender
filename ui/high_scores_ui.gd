extends Control


func _ready() -> void:
	update_scores()


func update_scores() -> void:
	var nodes = [$Scores/Place1, $Scores/Place2, $Scores/Place3, $Scores/Place4, $Scores/Place5]
	var last_run_place = HighScores.get_last_run_place()
	for i in range(len(nodes)):
		var node = nodes[i]
		var score = HighScores.scores[i]
		node.set_score_data(i+1, score)
		if last_run_place == i+1:
			node.set_bold(true)
		else:
			node.set_bold(false)

	if last_run_place < 6:
		$Scores/LastRun.visible = false
		return
	$Scores/LastRun.visible = true
	$Scores/LastRun.set_score_data(last_run_place, HighScores.last_run)
	$Scores/LastRun.set_bold(true)
