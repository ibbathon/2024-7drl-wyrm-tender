extends Control

signal continue_game
const SCORE_TEXT: String = "Score: %s"
const KILL_TEXT: String = "You murdered %s enemies"
const LOOT_TEXT: String = "You collected %s piles of gold"
const MISC_TEXT: String = "Miscellaneous stats:"


func _input(event: InputEvent) -> void:
	if not visible:
		return
	if event.is_action_released("ui_accept"):
		get_viewport().set_input_as_handled()
		continue_game.emit()


func update_stats(stats: Dictionary):
	var text: String = SCORE_TEXT % HighScores.current_run["score"]
	for keys in [
		["Mobs Killed", KILL_TEXT],
		["Gold Looted", LOOT_TEXT],
	]:
		if keys[0] in stats:
			if stats[keys[0]] > 0:
				text += "\n"
				text += keys[1] % stats[keys[0]]
			stats.erase(keys[0])
	if not stats.is_empty():
		text += "\n"
		text += MISC_TEXT
		for stat in stats.keys():
			text += "\n"
			text += stat + ": " + str(stats[stat])
	$Stats.text = text


func _on_continue_pressed() -> void:
	continue_game.emit()
