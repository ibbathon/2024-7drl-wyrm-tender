extends CanvasLayer

signal mouseover(info: TooltipInfo)
signal mouseoff

var HUDProgressBar = preload("res://ui/hud_progress_bar.tscn")
var pickup_type_to_image_coords = {
	ActorEnums.PickupType.DAGGER: Rect2i(
		0, 320, Constants.SPRITE_SIZE, Constants.SPRITE_SIZE
	),
	ActorEnums.PickupType.SMOKE_BOMB: Rect2i(
		128, 320, Constants.SPRITE_SIZE, Constants.SPRITE_SIZE
	),
}
var _inventory: Array[ActorEnums.PickupType]


func update_score(score: int):
	$PlayerStats/VBox/Stats/Score/Value.text = str(score)


func update_player_hp(cur_hp: int, max_hp: int):
	$PlayerStats/VBox/Stats/HP.update_progress(cur_hp, max_hp)


func update_goals(cur_goals: Array[Mission.Goal], opt_goals: Array[Mission.Goal]):
	for child in $Goals/VBox/GoalProgress.get_children():
		child.queue_free()

	for goal_set in [cur_goals, opt_goals]:
		for goal in goal_set:
			if goal.complete:
				continue
			_add_goal_separator()
			if goal.goal_type in [MapEnums.GoalType.kill_mob, MapEnums.GoalType.loot]:
				var progress_bar = HUDProgressBar.instantiate()
				progress_bar.update_progress(goal.progress, goal.quantity)
				goal.connect("progressed", progress_bar.update_progress)
				if goal.goal_type == MapEnums.GoalType.kill_mob:
					progress_bar.label_text = "Kill enemies"
				if goal.goal_type == MapEnums.GoalType.loot:
					progress_bar.label_text = "Collect gold"
				if goal_set == opt_goals:
					progress_bar.label_text += " (optional)"
				progress_bar.label_text += ":"
				$Goals/VBox/GoalProgress.add_child(progress_bar)
			elif goal.goal_type in [MapEnums.GoalType.exit]:
				var label = Label.new()
				label.text = "Reach the exit"
				$Goals/VBox/GoalProgress.add_child(label)
	_add_goal_separator()


func _add_goal_separator():
	var sep = VSeparator.new()
	sep.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	sep.add_theme_stylebox_override("separator", StyleBoxEmpty.new())
	$Goals/VBox/GoalProgress.add_child(sep)


func update_inventory(inventory: Array[ActorEnums.PickupType]):
	_inventory = inventory
	if inventory.is_empty():
		$PlayerStats/VBox/Inventory.hide()
		return
	$PlayerStats/VBox/Inventory.show()

	for i in range(Constants.MAX_INVENTORY_SIZE):
		var sprite_node: Sprite2D = get_node("PlayerStats/VBox/Inventory/Item" + str(i))
		if i < len(inventory):
			sprite_node.texture.region = pickup_type_to_image_coords[inventory[i]]
			sprite_node.show()
		else:
			sprite_node.hide()


func _inventory_mouseover(idx: int):
	if idx >= len(_inventory):
		return
	var info_name: String = ActorConstants.NAME_BY_PICKUP_TYPE[_inventory[idx]]
	var info_desc: String = ActorConstants.DESC_BY_PICKUP_TYPE[_inventory[idx]]

	mouseover.emit(TooltipInfo.new(info_name, info_desc))


func _inventory_mouseoff():
	mouseoff.emit()
