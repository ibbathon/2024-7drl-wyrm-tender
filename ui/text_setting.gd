@tool
extends HBoxContainer

@export var label_text: String:
	get:
		return $Label.text
	set(value):
		$Label.text = value

@export var default_value: String:
	get:
		return $LineEdit.text
	set(value):
		$LineEdit.text = value
