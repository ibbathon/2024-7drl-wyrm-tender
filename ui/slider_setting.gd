@tool
extends HBoxContainer

@export var label_text: String:
	get:
		return $Label.text
	set(value):
		$Label.text = value

@export var min_value: float:
	get:
		return $HBoxContainer/HSlider.min_value
	set(value):
		$HBoxContainer/HSlider.min_value = value

@export var max_value: float:
	get:
		return $HBoxContainer/HSlider.max_value
	set(value):
		$HBoxContainer/HSlider.max_value = value

@export var step_size: float:
	get:
		return $HBoxContainer/HSlider.step
	set(value):
		$HBoxContainer/HSlider.step = value

@export var default_value: float:
	get:
		return $HBoxContainer/HSlider.value
	set(value):
		$HBoxContainer/HSlider.value = value


func _ready():
	$HBoxContainer/LineEdit.text = str($HBoxContainer/HSlider.value)


func _on_h_slider_value_changed(_value):
	$HBoxContainer/LineEdit.text = str($HBoxContainer/HSlider.value)


func _on_line_edit_text_changed(_new_text):
	$HBoxContainer/HSlider.set_value_no_signal(_clamped_text_value())


func _on_line_edit_focus_exited():
	var clamped_val = _clamped_text_value()
	$HBoxContainer/HSlider.set_value_no_signal(clamped_val)
	$HBoxContainer/LineEdit.text = str(clamped_val)


func _clamped_text_value():
	var float_val = float($HBoxContainer/LineEdit.text)
	if float_val < $HBoxContainer/HSlider.min_value:
		float_val = $HBoxContainer/HSlider.min_value
	elif float_val > $HBoxContainer/HSlider.max_value:
		float_val = $HBoxContainer/HSlider.max_value
	return float_val
