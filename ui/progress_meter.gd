extends Control

func set_progress(step: String, pcnt: float):
	$MeterAndLabel/CurrentStep.text = step
	$MeterAndLabel/ProgressBar.value = pcnt
