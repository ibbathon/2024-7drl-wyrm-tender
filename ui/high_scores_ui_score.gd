extends HBoxContainer

func set_score_data(rank, score):
	$Place.text = str(rank)
	$Dragon.text = score["dragon"]
	$Kobold.text = score["kobold"]
	$Levels.text = str(score["levels"])
	$Score.text = str(score["score"])


func set_bold(bold: bool):
	if bold:
		$Place.add_theme_constant_override("outline_size", 2)
		$Dragon.add_theme_constant_override("outline_size", 2)
		$Kobold.add_theme_constant_override("outline_size", 2)
		$Levels.add_theme_constant_override("outline_size", 2)
		$Score.add_theme_constant_override("outline_size", 2)
	else:
		$Place.remove_theme_constant_override("outline_size")
		$Dragon.remove_theme_constant_override("outline_size")
		$Kobold.remove_theme_constant_override("outline_size")
		$Levels.remove_theme_constant_override("outline_size")
		$Score.remove_theme_constant_override("outline_size")
