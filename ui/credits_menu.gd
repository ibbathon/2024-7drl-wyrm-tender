extends Control

signal back_pressed


func _input(event: InputEvent) -> void:
	if not visible:
		return
	if event.is_action_released("ui_cancel"):
		get_viewport().set_input_as_handled()
		back_pressed.emit()


func _on_back_button_pressed() -> void:
	back_pressed.emit()


func _on_back_button_mouse_entered() -> void:
	$BackButton/CreditsAnimation.play("wiggle")


func _on_back_button_mouse_exited() -> void:
	$BackButton/CreditsAnimation.stop()
