extends Control

signal new_game
signal quit_game
signal resume_game

var new_game_settings: Dictionary
var ToggleSetting = preload("res://ui/toggle_setting.tscn")
var SliderSetting = preload("res://ui/slider_setting.tscn")
var Setting = preload("res://setting.gd")
var SettingType = Setting.SettingType

enum SettingGroup {
	ADVANCED_MAP_GEN,
}


func _ready():
	# randomly choose tile outline tileset
	var map_type = Data.map_types[Data.map_types.keys().pick_random()]
	$UpperLeft.tile_set.get_source(1).texture = map_type.texture
	$LowerRight.tile_set.get_source(1).texture = map_type.texture
	_on_resized()

	# dynamically fill out the options menu
	var map_types = {"Default": "null"}
	for mt in Data.map_types:
		map_types[mt] = mt
	var mission_types = {"Random": "random"}
	for mt in Data.mission_types:
		mission_types[mt] = mt
	$NewGameMenu/Settings.populate_from_settings({
		"Normal": [
			Setting.new(
				SettingType.TEXT, "name", "Name", "Random",
			),
		],
		"Advanced": [
			Setting.new(
				SettingType.DROPDOWN, "mission_type", "Mission Type", "random",
				Setting.DropdownSubsettings.new(mission_types),
			),
			Setting.new(
				SettingType.DROPDOWN, "map_type", "Override Map Type", "null",
				Setting.DropdownSubsettings.new(map_types),
			),
			Setting.new(
				SettingType.BOOL, "override_mapgen", "Override Mapgen Settings\nfrom Mission",
				false, null, -1, SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.PCNT, "room_frequency", "Room Frequency", 60,
				null, SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.SLIDER, "room_size", "Room Size", 20,
				Setting.SliderSubsettings.new(4, 100, 1), SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.SLIDER, "cave_size", "Cave Size", 5,
				Setting.SliderSubsettings.new(2, 100, 1), SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.SLIDER, "enemy_count", "Enemy Count", 10,
				Setting.SliderSubsettings.new(1, 100, 1), SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.SLIDER, "termite_count", "Termite Count", 8,
				Setting.SliderSubsettings.new(1, 20, 1), SettingGroup.ADVANCED_MAP_GEN,
			),
			Setting.new(
				SettingType.SLIDER, "termite_lifespan", "Termite Lifespan", 20,
				Setting.SliderSubsettings.new(1, 30, 1), SettingGroup.ADVANCED_MAP_GEN,
			),
		],
	})
	$OptionsMenu/Settings.populate_from_settings(GameSettings.full_settings)
	# load persisted settings into the options menu
	_load_options_from_global()


func _input(event: InputEvent) -> void:
	if not visible:
		return
	if event.is_action_released("ui_cancel"):
		if not in_base_menu():
			get_viewport().set_input_as_handled()
			$OptionsMenu.hide()
			$NewGameMenu.hide()
			$TopLevelMenu.show()
	elif event.is_action_released("ui_accept") or event.is_action_released("form_accept"):
		if (
			$NewGameMenu.visible and (
				event.is_action_released("form_accept") or
				not $NewGameMenu/Settings.is_text_box_focused()
			)
		):
			get_viewport().set_input_as_handled()
			_on_start_new_game_pressed()


func in_base_menu():
	return $TopLevelMenu.visible


func show_menu(game_in_progress: bool = false):
	visible = true
	$TopLevelMenu/Buttons/Resume.visible = game_in_progress


func _on_resume_pressed():
	resume_game.emit()


func _on_new_pressed():
	$TopLevelMenu.visible = false
	$NewGameMenu.visible = true


func _on_options_pressed():
	$TopLevelMenu.visible = false
	$OptionsMenu.visible = true


func _on_quit_pressed():
	quit_game.emit()


func _on_start_new_game_pressed():
	$TopLevelMenu.visible = true
	$NewGameMenu.visible = false
	var menu_game_settings = $NewGameMenu/Settings.get_all_values()
	var player_name = menu_game_settings["Normal"]["name"]
	if player_name == "Random":
		player_name = KoboldNames.gen_kobold_name()


	new_game_settings = {
		"map settings": Map.MapSettings.new(
			menu_game_settings["Advanced"]["map_type"],
			menu_game_settings["Advanced"]["room_frequency"],
			menu_game_settings["Advanced"]["room_size"],
			menu_game_settings["Advanced"]["cave_size"],
			menu_game_settings["Advanced"]["termite_count"],
			menu_game_settings["Advanced"]["termite_lifespan"],
		),
		"mission type": menu_game_settings["Advanced"]["mission_type"],
		"override maptype": menu_game_settings["Advanced"]["map_type"] != "null",
		"override mapgen": menu_game_settings["Advanced"]["override_mapgen"],
		"enemy count": menu_game_settings["Advanced"]["enemy_count"],
		"name": player_name,
	}
	new_game.emit()


func _on_new_game_back_pressed():
	$NewGameMenu.visible = false
	$TopLevelMenu.visible = true


func _load_options_from_global():
	$OptionsMenu/Settings.load_all_values(GameSettings.settings)


func _on_options_save_pressed():
	GameSettings.settings = $OptionsMenu/Settings.get_all_values()
	$OptionsMenu.visible = false
	$TopLevelMenu.visible = true


func _on_options_cancel_pressed():
	_load_options_from_global()
	$OptionsMenu.visible = false
	$TopLevelMenu.visible = true


func _on_resized() -> void:
	$LowerRight.position = size
	var tile_size: Vector2i = Vector2i(
		ceili(size.x / Constants.SPRITE_SIZE),
		ceili(size.y / Constants.SPRITE_SIZE),
	)
	for i in range(tile_size.x):
		var tile_top: Vector2i = _random_wall_tile()
		var tile_bot: Vector2i = _random_wall_tile()
		var cell_top: Vector2i = Vector2i(i, 0)
		var cell_bot: Vector2i = Vector2i(-i-1, -1)
		if $UpperLeft.get_cell_atlas_coords(cell_top) == -1 * Vector2i.ONE:
			$UpperLeft.set_cell(cell_top, MapConstants.TILESET_SOURCE, tile_top)
		if $LowerRight.get_cell_atlas_coords(cell_bot) == -1 * Vector2i.ONE:
			$LowerRight.set_cell(cell_bot, MapConstants.TILESET_SOURCE, tile_bot)
	for i in range(tile_size.y):
		var tile_left: Vector2i = _random_wall_tile()
		var tile_right: Vector2i = _random_wall_tile()
		var cell_left: Vector2i = Vector2i(0, i)
		var cell_right: Vector2i = Vector2i(-1, -i-1)
		if $UpperLeft.get_cell_atlas_coords(cell_left) == -1 * Vector2i.ONE:
			$UpperLeft.set_cell(cell_left, MapConstants.TILESET_SOURCE, tile_left)
		if $LowerRight.get_cell_atlas_coords(cell_right) == -1 * Vector2i.ONE:
			$LowerRight.set_cell(cell_right, MapConstants.TILESET_SOURCE, tile_right)


func _random_wall_tile() -> Vector2i:
	var tile: Vector2i = MapConstants.terrain_to_tileset_default[MapEnums.TerrainType.WALL]
	var tile_variants: Array[Vector2i] = (
		MapConstants.terrain_to_tileset_variants[MapEnums.TerrainType.WALL]
	)
	if randi_range(0,99) <= MapConstants.TILE_VARIANT_FREQUENCY:
		tile = tile_variants.pick_random()
	return tile


func _on_button_mouse_entered(node_path: NodePath) -> void:
	var node: Control = get_node(node_path)
	$HoverIcon.global_position = node.global_position


func _on_button_mouse_exited() -> void:
	$HoverIcon.global_position = Vector2.ZERO


func _on_credits_menu_back_pressed() -> void:
	$CreditsMenu.hide()


func _on_credits_button_pressed() -> void:
	$CreditsMenu.show()


func _on_credits_button_mouse_entered() -> void:
	$CreditsButton/CreditsAnimation.play("wiggle")


func _on_credits_button_mouse_exited() -> void:
	$CreditsButton/CreditsAnimation.stop()


## Button which forces a reload of the settings file. This is to allow editing hidden settings.
func _on_force_reload_button_pressed() -> void:
	GameSettings.load_from_file()
	$OptionsMenu/Settings.populate_from_settings(GameSettings.full_settings)
