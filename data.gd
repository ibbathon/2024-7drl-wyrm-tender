extends Node

var map_types: Dictionary = {}
var mission_types: Dictionary = {}
var mission_frequency_list: Array = []  # Array[Array[int, mission]]
var mob_types: Dictionary = {}


func _ready():
	var data_files = DirAccess.open("res://data").get_files()
	for filename in data_files:
		# TODO: These could probably be generalized.
		if filename.ends_with(".maptype.json"):
			var data = Util.load_json_file("res://data/" + filename)
			if "error" not in data:
				var map_type = MapType.new(filename, data)
				map_types[filename.get_file().get_basename().get_basename()] = map_type
		elif filename.ends_with(".mission.json"):
			var data = Util.load_json_file("res://data/" + filename)
			if "error" not in data:
				var mission = MissionType.new(filename, data)
				mission_types[filename.get_file().get_basename().get_basename()] = mission

	# TODO: generate these from files, rather than hard-coded
	mob_types["knight"] = MobType.new("knight.mobtype.json", {"plural_name": "knights"})
	mob_types["rat"] = MobType.new("rat.mobtype.json", {"plural_name": "rats"})
	mob_types["civilian"] = MobType.new("civilian.mobtype.json", {"plural_name": "civilians"})

	var freq_total: int = 0
	for mission_type in mission_types:
		freq_total += mission_types[mission_type].mission_frequency
		mission_frequency_list.append([freq_total, mission_type])
