extends Node
class_name TooltipInfo

var header: String
var text: String


func _init(header_: String, text_: String):
	header = header_
	text = text_
