extends Actor
class_name Player

signal inventory_updated(inventory)

var mission: Mission
var exit_loc: Vector2i
var inventory: Array[ActorEnums.PickupType] = []
var _invisible_timer: int = 0
const SMOKE_BOMB_TURNS: int = 3
var invisible: bool:
	get: return _invisible_timer > 0
	set(value):
		if value: _invisible_timer = SMOKE_BOMB_TURNS
		else: _invisible_timer = 0
		$SmokeCloud.visible = value


func update_indicators():
	if mission == null:
		return
	# point the goal indicator in the right direction
	var target: Vector2i = exit_loc
	var current_goals = mission.get_current_goals()
	if current_goals.is_empty():
		return
	var current_goal_type = current_goals[0].goal_type
	var goal_group: String = "invalid"
	if current_goal_type == MapEnums.GoalType.kill_mob:
		goal_group = "mob"
	elif current_goal_type == MapEnums.GoalType.loot:
		goal_group = "gold"
	var nearest_target = _find_nearest_target(goal_group)
	if nearest_target != Vector2i.ZERO:
		target = nearest_target
	Util.log_pathing("player target loc " + str(target), 2)


	var path = map.get_point_path(location, target)
	if path.is_empty():
		return
	var point_before_wall = path[0]
	for point in path:
		var center_point = point + Vector2.ONE * Constants.SPRITE_SIZE / 2
		$SightLine.target_position = center_point - position
		$SightLine.global_rotation = 0
		$SightLine.force_raycast_update()
		if $SightLine.is_colliding():
			break
		point_before_wall = center_point
	# We know the nearest point without hitting a wall, so angle the indicator in that direction
	var angle = (point_before_wall - position).angle()
	$GoalIndicator.global_rotation = angle


func decide_next_move():
	# handle the actual input which will determine our next move
	if Input.is_action_pressed("move_right"):
		return Vector2i(1, 0)
	elif Input.is_action_pressed("move_left"):
		return Vector2i(-1, 0)
	elif Input.is_action_pressed("move_down"):
		return Vector2i(0, 1)
	elif Input.is_action_pressed("move_up"):
		return Vector2i(0, -1)
	elif Input.is_action_pressed("ui_accept"):
		return Vector2i(0, 0)
	else:
		return null


func mark_dead():
	animation = "dead"
	super()


func _find_nearest_target(target_group: String) -> Vector2i:
	var nearest_dist: float = INF
	var nearest_target: Actor = null
	var targets = get_tree().get_nodes_in_group(target_group)
	if targets.is_empty():
		return Vector2i.ZERO

	for target: Actor in targets:
		if target.dead or target.is_queued_for_deletion():
			continue
		var path = map.get_id_path(location, target.location)
		if len(path) < nearest_dist:
			nearest_target = target
			nearest_dist = len(path)

	if nearest_target == null:
		return Vector2i.ZERO

	return nearest_target.location


func _on_tooltip_body_mouse_entered() -> void:
	mouseover.emit(
		TooltipInfo.new(
			actor_name,
			"It's you!\nHP: %s/%s" % [cur_hp, max_hp]
		)
	)


func add_to_inventory(pickup_type: ActorEnums.PickupType):
	if has_inventory_space() and pickup_type != ActorEnums.PickupType.SCORE_ONLY:
		inventory.append(pickup_type)
		inventory_updated.emit(inventory)


func has_inventory_space() -> bool:
	return len(inventory) < Constants.MAX_INVENTORY_SIZE


func has_item(pickup_type: ActorEnums.PickupType) -> bool:
	for i in range(len(inventory)):
		if inventory[i] == pickup_type:
			return true
	return false


func use_item(pickup_type: ActorEnums.PickupType) -> bool:
	for i in range(len(inventory)):
		if inventory[i] == pickup_type:
			inventory.remove_at(i)
			inventory_updated.emit(inventory)
			return true
	return false


func empty_inventory():
	inventory = []
	inventory_updated.emit(inventory)


func decrement_invisible_timer():
	if _invisible_timer > 0:
		_invisible_timer -= 1
		if _invisible_timer == 0:
			$SmokeCloud.hide()
