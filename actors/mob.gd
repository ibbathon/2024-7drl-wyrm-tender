extends Actor
class_name Mob

enum AlertLevel {
	UNALERTED,
	SEARCHING,
	ALERT,
}
var alert_symbols = {
	AlertLevel.SEARCHING: Rect2i(64, 448, 64, 64),
	AlertLevel.ALERT: Rect2i(0, 448, 64, 64),
}
var alert_colors = {
	AlertLevel.UNALERTED: Color(0, 1, 0, 0.2),
	AlertLevel.SEARCHING: Color(1, 1, 0, 0.2),
	AlertLevel.ALERT: Color(1, 0, 0, 0.2),
}

var patrol_path: PackedVector2Array = []
var patrol_index: int = 0
var next_target_loc: Vector2i
var player_spotted: bool = false
var target_player: Actor
var raging_ai: bool = false
var unalerted_vision_angle: float = PI / 6
var unalerted_vision_distance: float = 3 * Constants.SPRITE_SIZE
var alerted_vision_angle: float = PI / 3
var alerted_vision_distance: float = 4 * Constants.SPRITE_SIZE
var alerted_radial_vision_distance: float = 1.0 * Constants.SPRITE_SIZE
var turns_without_spotting_player: int = 0
var turns_dedicated_to_hunt: int = 4
var vision_angle:
	get: return alerted_vision_angle if player_spotted else unalerted_vision_angle
var vision_distance:
	get:
		var dist = alerted_vision_distance if player_spotted else unalerted_vision_distance
		if raging_ai:
			return dist * 2
		else:
			return dist


func _init():
	score_value = 1


func _ready():
	super()
	GameSettings.settings_changed.connect(_toggle_ai_lines)
	_toggle_ai_lines()
	GameSettings.settings_changed.connect(_adjust_sight_cone)
	_adjust_sight_cone()


func _process(_delta: float) -> void:
	# It's easier to just force the DebugInfo onto global transform than fiddle with inverting it
	# for readability/accuracy.
	$DebugInfo.global_transform = Transform2D()


func set_random_name() -> void:
	actor_name = KnightNames.gen_knight_name()


func update_facing(move_direction: Vector2i):
	super(move_direction)
	$SightCone.rotation = 0
	$SightCone.scale = Vector2.ONE
	if facing == Facing.UP:
		$SightCone.rotation = -PI/2
	elif facing == Facing.DOWN:
		$SightCone.rotation = PI/2
	elif facing == Facing.LEFT:
		$SightCone.scale = Vector2(-1, 1)


func _toggle_ai_lines():
	$DebugInfo.visible = GameSettings.get_setting_by_name("show_ai_lines")


func _adjust_sight_cone():
	# sight cone is an arc from -vision_angle to +vision_angle
	# The below will generate a bunch of points so the polygon doesn't look terrible.
	for i in range(1, len($SightCone.polygon)):
		var point = (Vector2.RIGHT * vision_distance).rotated(
			-vision_angle + (i-1) * 2 * vision_angle / (len($SightCone.polygon) - 2)
		)
		$SightCone.polygon[i] = point


func decide_next_move() -> Vector2i:
	$DebugInfo/TowardTarget.points[0] = position
	$DebugInfo/MoveDir.points[0] = $DebugInfo/TowardTarget.points[0]
	$DebugInfo/TowardTarget.points[1] = $DebugInfo/TowardTarget.points[0]
	$DebugInfo/MoveDir.points[1] = $DebugInfo/MoveDir.points[0]

	if player_spotted:
		next_target_loc = target_player.location
	else:
		# just on patrol
		next_target_loc = patrol_path[patrol_index]
		if location == Vector2i(next_target_loc):
			patrol_index = (patrol_index + 1) % len(patrol_path)
			next_target_loc = patrol_path[patrol_index]

	# We now know where we want to be. Set the debug info and calculate optimal path.
	$DebugInfo/TowardTarget.points[1] = (
		Vector2(next_target_loc * Constants.SPRITE_SIZE) + Vector2.ONE * Constants.SPRITE_SIZE / 2
	)
	var point_path = map.get_id_path(location, next_target_loc)
	if len(point_path) > 1:
		var move_dir = point_path[1] - location
		$DebugInfo/MoveDir.points[1] = (
			$DebugInfo/MoveDir.points[0] + Vector2(move_dir) * Constants.SPRITE_SIZE / 2
		)
		return move_dir
	return Vector2i.ZERO


## Builds a patrol path by incrementally increasing the size of a rectangle around the mob until
## it encounters walls or reaches a certain size in each dimension. Once it has reached an obstacle
## in one direction, it will still continue in other directions.
func build_patrol_path(
	max_path_distance: int = 5,
) -> void:
	Util.log_ai(str(self) + " building patrol path", 1)
	var directions = [
		Vector2i.LEFT,
		Vector2i.RIGHT,
		Vector2i.UP,
		Vector2i.DOWN,
	]
	var patrol_rect = Rect2i(location.x, location.y, 0, 0)

	for i in range(max_path_distance):
		var obstructed_directions = []
		for direction in directions:
			if direction in obstructed_directions:
				continue

			# build a search rect based on the understanding that we've already checked all
			# the points in the patrol_rect, so we only need to check the ones that would be
			# added by expanding in the given direction
			var search_rect: Rect2i
			if direction == Vector2i.LEFT:
				search_rect = Rect2i(
					patrol_rect.position + direction,
					Vector2i(0, patrol_rect.size.y),
				)
			elif direction == Vector2i.RIGHT:
				search_rect = Rect2i(
					Vector2i(patrol_rect.end.x + 1, patrol_rect.position.y),
					Vector2i(0, patrol_rect.size.y),
				)
			elif direction == Vector2i.UP:
				search_rect = Rect2i(
					patrol_rect.position + direction,
					Vector2i(patrol_rect.size.x, 0),
				)
			elif direction == Vector2i.DOWN:
				search_rect = Rect2i(
					Vector2i(patrol_rect.position.x, patrol_rect.end.y + 1),
					Vector2i(patrol_rect.size.x, 0),
				)

			# check each of those points for a solid point on the map, marking this direction
			# as obstructed if any are solid
			for x in range(search_rect.position.x, search_rect.end.x + 1):
				for y in range(search_rect.position.y, search_rect.end.y + 1):
					if not map.is_in_boundsv(Vector2i(x, y)) or map.is_point_solid(Vector2i(x, y)):
						obstructed_directions.append(direction)
						break
				if direction in obstructed_directions:
					break

			# finally, if we didn't encounter any solid points, expand the patrol_rect
			if direction not in obstructed_directions:
				if direction.abs() == direction:
					patrol_rect.end += direction
				else:
					patrol_rect.position += direction
					patrol_rect.size += direction.abs()

	# we have now constructed a patrol_rect with the maximum size; convert it to a patrol path
	patrol_path.clear()
	patrol_path.append_array([
		patrol_rect.position,
		patrol_rect.position + Vector2i.RIGHT * patrol_rect.size.x,
		patrol_rect.end,
		patrol_rect.position + Vector2i.DOWN * patrol_rect.size.y,
	])
	# and select a random point on that path for the starting point
	patrol_index = randi_range(0, patrol_path.size() - 1)
	$DebugInfo/PatrolPath.clear_points()
	for point in patrol_path:
		$DebugInfo/PatrolPath.add_point(
			point * Constants.SPRITE_SIZE + Vector2.ONE * Constants.SPRITE_SIZE / 2
		)


func play_footstep_sounds():
	$FootstepSounds.play()


func _set_alert_level(level: AlertLevel):
	$SightCone.color = alert_colors[level]
	if level == AlertLevel.UNALERTED:
		$AlertSymbol.hide()
		armor = 0
		return
	armor = 1
	$AlertSymbol.show()
	$AlertSymbol.texture.region = alert_symbols[level]


func update_ai():
	var nearest_player = _find_nearest_visible_player(map)
	if nearest_player != null:
		Util.log_ai(str(self) + " player seen", 1)
		turns_without_spotting_player = 0
		player_spotted = true
		target_player = nearest_player
		_set_alert_level(AlertLevel.ALERT)
		_adjust_sight_cone()
	elif target_player != null:
		turns_without_spotting_player += 1
		_set_alert_level(AlertLevel.SEARCHING)
		if turns_without_spotting_player >= turns_dedicated_to_hunt:
			Util.log_ai(str(self) + " giving up", 1)
			forget_player()
		Util.log_ai(str(self) + " searching", 1)


func update_indicators():
	decide_next_move()


## Returns the nearest player of the ones visible to this mob.
## Returns null if no players are visible.
func _find_nearest_visible_player(_map: AStarGrid2D):
	var players = get_tree().get_nodes_in_group("player")
	if players.is_empty():
		return null

	var nearest_player: Player
	var nearest_distance: float = INF
	for player: Player in players:
		if player.invisible:
			Util.log_ai(str(self) + " player invisible", 2)
			continue

		var player_vector: Vector2 = player.position - position
		$DebugInfo/SightLine.points[0] = position
		$DebugInfo/SightLine.points[1] = player.position
		$SightLine.target_position = player_vector
		$SightLine.global_rotation = 0
		$SightLine.force_raycast_update()
		var distance: float = player_vector.length()
		# If the player is within the radial vision distance, then they're automatically seen.
		if player_spotted and distance <= alerted_radial_vision_distance:
			Util.log_pathing(str(self) + " within radial", 2)
		else:
			# Otherwise, check the vision cone.
			var angle = abs(player_vector.angle_to(FACING_TO_SIGHT_DIR[facing]))
			if angle > vision_angle:
				Util.log_pathing(str(self) + " player angle", 2)
				continue
			if distance > vision_distance:
				Util.log_pathing(str(self) + " player distance", 2)
				continue
			if $SightLine.is_colliding():
				Util.log_pathing(str(self) + " player obscured", 2)
				continue
		if distance < nearest_distance:
			nearest_distance = distance
			nearest_player = player

	return nearest_player


func _on_tooltip_body_mouse_entered() -> void:
	var armortext: String
	if player_spotted:
		armortext = "Armor: 1 (0 when unalerted)"
	else:
		armortext = "Armor: 0 (1 when alerted)"

	mouseover.emit(
		TooltipInfo.new(
			actor_name,
			"Vile human scum.\n" +
			"HP: %s/%s\n" % [cur_hp, max_hp] +
			armortext
		)
	)


func forget_player():
	player_spotted = false
	target_player = null
	_set_alert_level(AlertLevel.UNALERTED)
	_adjust_sight_cone()
