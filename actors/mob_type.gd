extends Node
class_name MobType

# TODO: actually fill this out
var filename: String
var plural_name: String


func _init(filename_: String, json_data: Dictionary):
	filename = filename_
	plural_name = json_data["plural_name"]


func _to_string() -> String:
	return filename
