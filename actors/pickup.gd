extends Actor
class_name Pickup

@export var pickup_type: ActorEnums.PickupType:
	get: return pickup_type
	set(value):
		pickup_type = value
		animation = _type_to_animation[pickup_type]
		actor_name = _type_to_name[pickup_type]
		score_value = _type_to_score[pickup_type]
		if pickup_type == ActorEnums.PickupType.SCORE_ONLY:
			add_to_group("gold")
		else:
			remove_from_group("gold")

var _type_to_animation: Dictionary = {
	ActorEnums.PickupType.SCORE_ONLY: "gold",
	ActorEnums.PickupType.DAGGER: "dagger",
	ActorEnums.PickupType.SMOKE_BOMB: "smokebomb",
}
var _type_to_name: Dictionary = {
	ActorEnums.PickupType.SCORE_ONLY: "Pile of Gold",
	ActorEnums.PickupType.DAGGER: "Dagger",
	ActorEnums.PickupType.SMOKE_BOMB: "Smoke Bomb",
}
var _type_to_score: Dictionary = {
	ActorEnums.PickupType.SCORE_ONLY: 2,
	ActorEnums.PickupType.DAGGER: 0,
	ActorEnums.PickupType.SMOKE_BOMB: 0,
}

func _init():
	score_value = 2


func update_facing(_move_direction: Vector2i):
	# overriding because pickups should not have facing
	pass


func _fix_sprite_facing():
	# overriding because pickups should not have facing
	pass


func _on_tooltip_body_mouse_entered() -> void:
	var info_name: String = ActorConstants.NAME_BY_PICKUP_TYPE[pickup_type]
	var info_text: String = ActorConstants.DESC_BY_PICKUP_TYPE[pickup_type]

	mouseover.emit(TooltipInfo.new(info_name, info_text))
