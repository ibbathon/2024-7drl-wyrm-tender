extends Node
class_name ActorConstants

const DESC_GOLD = "A pile of gold you can lug back to your mistress."
const DESC_DAGGER = (
	"A nice throwing dagger for dispatching foes at a distance. "
	+ "Click an enemy to throw the dagger."
)
const DESC_SMOKE_BOMB = (
	"A handy smoke bomb for getting out of tight scrapes. "
	+ "Click the player to use the smoke bomb."
)
const NAME_GOLD = "Pile of Gold"
const NAME_DAGGER = "Dagger"
const NAME_SMOKE_BOMB = "Smoke Bomb"

const NAME_BY_PICKUP_TYPE: Dictionary = {
	ActorEnums.PickupType.SCORE_ONLY: NAME_GOLD,
	ActorEnums.PickupType.DAGGER: NAME_DAGGER,
	ActorEnums.PickupType.SMOKE_BOMB: NAME_SMOKE_BOMB,
}

const DESC_BY_PICKUP_TYPE: Dictionary = {
	ActorEnums.PickupType.SCORE_ONLY: DESC_GOLD,
	ActorEnums.PickupType.DAGGER: DESC_DAGGER,
	ActorEnums.PickupType.SMOKE_BOMB: DESC_SMOKE_BOMB,
}
