extends Node
class_name KoboldNames

const main_consonants: Array[String] = ["", "b", "d", "f", "g", "h", "k", "n", "t"]
const mid_consonants: Array[String] = ["", "", "", "l", "r"]
const vowels: Array[String] = ["a", "i", "e", "o"]


static func gen_kobold_name() -> String:
	if randf() < 0.02:
		return "Goob"
	return _gen_syllable().capitalize()


static func gen_dragon_name():
	if randf() < 0.02:
		return "Glerp"
	return (_gen_syllable() + _gen_syllable() + _gen_syllable()).capitalize()


static func _gen_syllable() -> String:
	return (
		main_consonants.pick_random()
		+ mid_consonants.pick_random()
		+ vowels.pick_random()
		+ mid_consonants.pick_random()
		+ main_consonants.pick_random()
	)
