extends AnimatedSprite2D
class_name Actor

signal died
signal damaged
signal mouseover(info: TooltipInfo)
signal mouseoff
signal clicked(actor: Actor)

enum Facing {
	UP,
	DOWN,
	LEFT,
	RIGHT,
}
const FACING_TO_SIGHT_DIR = {
	Facing.UP: Vector2.UP,
	Facing.DOWN: Vector2.DOWN,
	Facing.LEFT: Vector2.LEFT,
	Facing.RIGHT: Vector2.RIGHT,
}
@export var max_hp: int = 1
@export var damage: int = 1
@export var armor: int = 0
@export var move_priority: int = 1 # lower moves first
@export var display_name: String = "Unknown"
@export var death_sound: AudioStreamWAV
@onready var death_sound_player: AudioStreamPlayer = $DeathSound
var cur_hp: int
var location: Vector2i
var sprite: Array = [1, Vector2i(15, 3), 0]
var dead: bool = false
var facing = Facing.RIGHT
var map: AStarGrid2D
var actor_name: String = "Actor"
var score_value: int = 1


func _ready():
	$DeathSound.stream = death_sound
	cur_hp = max_hp
	GameSettings.settings_changed.connect(_fix_sprite_facing)


func _to_string():
	return actor_name


## Initializes all non-defaulted parameters.
## MUST be called before any cycle work with the actor.
func initialize(loc: Vector2i, hp: int = -1):
	location = loc
	if hp >= 0:
		cur_hp = hp


## Determines the desired offset of movement (e.g. -1,1 for 1 left, 1 down).
## [param map] is a 2D array of TerrainType, representing the map of the level,
## with all obstructions.
func decide_next_move() -> Vector2i:
	return Vector2i.ZERO


## Performs appropriate action on the other actor (damage, push, etc.).
## [param other_actor] is the other actor to act on.
func act_on_actor(_other_actor: Actor):
	pass


func mark_dead():
	death_sound_player.play()
	dead = true
	died.emit()


## Helper method which waits for death sounds to finish before queue_free.
## Makes sprite invisible while waiting.
func ready_for_queue_free():
	visible = false
	death_sound_player.connect("finished", queue_free)


func move_to(new_loc: Vector2i, new_pos: Vector2) -> Tween:
	location = new_loc
	if not is_node_ready() or (new_pos - position).length_squared() > 17000:
		position = new_pos
		return
	var tween = get_tree().create_tween()
	tween.tween_property(self, "position", new_pos, 0.1)
	return tween


func attack_at(attack_loc: Vector2i) -> Tween:
	if not is_node_ready():
		return
	var original_position: Vector2 = position
	var original_rotation: float = rotation
	var pos_tweak: Vector2 = (attack_loc - location) * 10
	var rot_tweak: float = 0.5
	if pos_tweak.x < 0:
		rot_tweak = -rot_tweak
	var tween = get_tree().create_tween()
	tween.tween_property(self, "position", position + pos_tweak, 0.1)
	tween.parallel().tween_property(self, "rotation", rotation + rot_tweak, 0.1)
	tween.tween_property(self, "rotation", original_rotation, 0.1)
	tween.parallel().tween_property(self, "position", original_position, 0.1)
	return tween


func bounce_in_place() -> Tween:
	if not is_node_ready():
		return
	var tween = get_tree().create_tween()
	tween.tween_property(self, "rotation", rotation + 0.05, 0.05)
	tween.tween_property(self, "rotation", rotation - 0.05, 0.05)
	tween.tween_property(self, "rotation", rotation, 0.05)
	return tween


func update_facing(move_direction: Vector2i):
	if move_direction == Vector2i.ZERO:
		return

	flip_h = false
	animation = "default"
	var facing_up = "facing_up"
	if facing_up not in sprite_frames.get_animation_names():
		facing_up = "default"
	if move_direction.x > 0:
		facing = Facing.RIGHT
	elif move_direction.y < 0:
		facing = Facing.UP
		animation = facing_up
	elif move_direction.y > 0:
		facing = Facing.DOWN
	else:
		facing = Facing.LEFT
		flip_h = true


func _fix_sprite_facing():
	animation = "default"
	flip_h = false
	rotation = 0
	if facing == Facing.UP:
		update_facing(Vector2i.UP)
	elif facing == Facing.DOWN:
		update_facing(Vector2i.DOWN)
	elif facing == Facing.LEFT:
		update_facing(Vector2i.LEFT)
	elif facing == Facing.RIGHT:
		update_facing(Vector2i.RIGHT)
	update_indicators()


func play_footstep_sounds():
	pass


func reset_sprite():
	animation = "default"
	flip_h = false
	dead = false
	rotation = 0


func update_indicators():
	pass


## Handles getting hit by another actor.
func receive_damage(_from_actor: Actor, dmg: int):
	cur_hp = max(0, cur_hp - max(0, dmg - armor))
	if cur_hp == 0:
		mark_dead()
	damaged.emit()


func _on_tooltip_body_mouse_entered() -> void:
	mouseover.emit(TooltipInfo.new(actor_name, "HP: %s/%s" % [cur_hp, max_hp]))


func _on_tooltip_body_mouse_exited() -> void:
	mouseoff.emit()


func _on_tooltip_body_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_released("click"):
		clicked.emit(self)
