extends Node
class_name KnightNames

const syllables: Array[String] = [
	"blood",
	"worm",
	"worth",
	"hunt",
	"thud",
	"lurk",
	"death",
	"bleed",
	"punt",
	"quirk",
	"dead",
	"bird",
	"brain",
	"tool",
	"ghost",
	"smith",
]
const freq_syl_count: Array[int] = [0, 30, 90, 100]

const titles: Array[String] = [
	"Sir %s",
	"%s the Hunter",
	"Lord %s",
	"%s the Tainted",
	"%s the Unworthy",
]

const suffixes: Array[String] = [
	"son",
	"er",
	"dottir",
	"sman",
]

const FREQ_LAST_NAME: float = 0.5
const FREQ_SUFFIX: float = 0.25
const FREQ_TITLE: float = 0.25


static func gen_knight_name() -> String:
	if randf() < 0.02:
		return "Sir Bleedsalot"

	var knight_name: String = ""
	# first name
	var syl_count: int = _rand_syl_count()
	for i in range(syl_count):
		knight_name += syllables.pick_random()
	# last name
	if randf() < FREQ_LAST_NAME:
		knight_name += " "
		syl_count = _rand_syl_count()
		for i in range(syl_count):
			knight_name += syllables.pick_random()
		if syl_count < 3 and randf() < FREQ_SUFFIX:
			knight_name += suffixes.pick_random()

	knight_name = knight_name.capitalize()
	# title
	if randf() < FREQ_TITLE:
		knight_name = titles.pick_random() % knight_name

	return knight_name


static func _rand_syl_count() -> int:
	var freq = randi_range(1, 100)
	for i in range(len(freq_syl_count)):
		if freq_syl_count[i] > freq:
			return i
	return 1
