extends Node
class_name ActorEnums


enum PickupType {
	SCORE_ONLY,
	DAGGER,
	SMOKE_BOMB,
}
