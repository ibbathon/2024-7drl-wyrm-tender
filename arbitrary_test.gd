extends Node

func _ready():
	for i in range(20):
		var mission_type: MissionType
		var mission_rand: int = randi_range(0, Data.mission_frequency_list[-1][0])
		for mission_freq in Data.mission_frequency_list:
			if mission_rand < mission_freq[0]:
				mission_type = Data.mission_types[mission_freq[1]]
				break
		print(mission_type.filename)
