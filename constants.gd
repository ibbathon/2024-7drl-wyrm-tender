extends Node

const SPRITE_SIZE: int = 64

const DEFAULT_DRAGON_NAME: String = "Glerp"
const DEFAULT_KOBOLD_NAME: String = "Goob"

const MAX_INVENTORY_SIZE: int = 3
