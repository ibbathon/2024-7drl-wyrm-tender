extends CanvasLayer

var size: Vector2:
	get: return $Control.size


func set_info(info: TooltipInfo):
	$Control/VBoxContainer/Title.text = info.header
	$Control/VBoxContainer/Text.text = info.text
	$Control/VBoxContainer.reset_size()
	$Control.size = $Control/VBoxContainer.size + Vector2.ONE * 10
