@tool
extends CanvasItem

## The number of cells in the drawn grid.
## e.g. grid_size = 2 would yield
## -----
## | | |
## -----
## | | |
## -----
@export var grid_size: Vector2i
@export var cell_size: Vector2
@export var line_width: float = 3.0
@export var color: Color = Color("BLACK")


func _draw():
	var line_points: PackedVector2Array = []
	line_points.resize(2 * (grid_size.x + 1) + 2 * (grid_size.y + 1))
	for i in range(grid_size.x + 1):
		line_points[2*i] = Vector2(i * cell_size.x, 0)
		line_points[2*i+1] = Vector2(i * cell_size.x, grid_size.y * cell_size.y)
	var offset = 2 * (grid_size.x + 1)
	for j in range(grid_size.y + 1):
		line_points[offset + 2*j] = Vector2(0, j * cell_size.y)
		line_points[offset + 2*j+1] = Vector2(grid_size.x * cell_size.x, j * cell_size.y)

	draw_multiline(line_points, color, line_width)
