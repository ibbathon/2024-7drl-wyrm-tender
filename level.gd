extends Node2D

signal player_died
signal score_updated
signal level_complete(stats: Dictionary)

enum ActionResult {
	STAY,
	MOVE,
	ATTACK,
	ACQUIRE,
	MOVE_AND_PICKUP,
	EXIT,
	USE_ITEM,
}

class Action:
	var result: ActionResult
	var actee: Actor
	var additional_info: Array

	func _init(result_: ActionResult, actee_: Actor, additional_info_: Array = []):
		result = result_
		actee = actee_
		additional_info = additional_info_


@export var mob_scene: PackedScene
@export var gold_scene: PackedScene
const MAX_MOVE_ATTEMPTS = 3
const MAX_SPAWN_ATTEMPTS = 30

var map: Map
var navgrid: AStarGrid2D
var stats: Dictionary
var mission: Mission
var queued_action: Action = null
var move_count: int = 0
var _running_animations: Array[Tween] = []
var _indicators_need_updating: bool = true


func _ready():
	$HUD.update_player_hp($Player.cur_hp, $Player.max_hp)
	_toggle_fog_of_war()
	GameSettings.settings_changed.connect(_toggle_fog_of_war)
	GameSettings.settings_changed.connect(_toggle_grid_lines)
	_toggle_grid_lines()
	$Player.connect("inventory_updated", $HUD.update_inventory)


func _unhandled_input(event: InputEvent) -> void:
	if GameSettings.get_setting_by_name("allow_zoom"):
		if event.is_action_pressed("zoom_in"):
			_zoom_camera(0.1)
		if event.is_action_pressed("zoom_out"):
			_zoom_camera(-0.1)
		if event.is_action_pressed("zoom_reset"):
			$Player/Camera2D.zoom = Vector2.ONE
	if is_instance_of(event, InputEventMouseMotion):
		var offset = Vector2.ONE * 10
		if event.position.x > get_viewport().size.x / 2:
			offset.x = -$Tooltip.size.x - 2
		if event.position.y > get_viewport().size.y / 2:
			offset.y = -$Tooltip.size.y - 2
		$Tooltip.offset = event.position + offset


func _zoom_camera(amount: float) -> void:
	var zoom = $Player/Camera2D.zoom.x + amount
	zoom = clamp(zoom, 0.25, 2.0)
	$Player/Camera2D.zoom = Vector2.ONE * zoom


func set_player_name(name_: String):
	$Player.actor_name = name_


func reset(new_player: bool = false):
	Util.log_process("resetting level", 1)
	for tween: Tween in _running_animations:
		tween.kill()
	move_count = 0
	for actor in get_tree().get_nodes_in_group("actor"):
		if actor != $Player and actor != $ExitSign:
			actor.queue_free()
	stats = {
		"Mobs Killed": 0,
		"Gold Looted": 0,
	}
	$HUD.update_goals(mission.get_current_goals(), mission.get_optional_goals())
	mission.connect("goal_complete", _on_goal_complete)
	mission.connect("stage_complete", _on_stage_complete)
	$Player.mission = mission
	$Player.exit_loc = map.exit_loc

	$Tiles/Terrain.clear()
	$Player.reset_sprite()
	if new_player:
		$Player.cur_hp = $Player.max_hp
		$HUD.update_player_hp($Player.cur_hp, $Player.max_hp)
		$Player.empty_inventory()

	# Determine map type setup before going any further.
	var map_type: MapType = map.map_settings["map_type"]
	$Tiles/Terrain.tile_set.get_source(1).texture = map_type.texture

	# Adjust the size of the debugging grid lines based on the map settings
	$DebugLines/CaveGrid.grid_size = Vector2i.ONE * map.map_settings.cave_size
	$DebugLines/CaveGrid.cell_size = (
		Vector2i.ONE * map.map_settings.room_size * Constants.SPRITE_SIZE
	)
	$DebugLines/TileGrid.grid_size = (
		Vector2i.ONE * map.map_settings.cave_size * map.map_settings.room_size
	)
	$DebugLines/TileGrid.cell_size = Vector2i.ONE * Constants.SPRITE_SIZE

	navgrid = AStarGrid2D.new()
	navgrid.set_diagonal_mode(AStarGrid2D.DiagonalMode.DIAGONAL_MODE_NEVER)
	navgrid.region = Rect2i(0, 0, len(map.terrain[0]), len(map.terrain))
	navgrid.cell_size = Vector2(Constants.SPRITE_SIZE, Constants.SPRITE_SIZE)
	navgrid.update()
	$Player.map = navgrid

	Util.log_process("building tilemap", 1)
	$Tiles/Terrain.clear()
	$Tiles/Walls.clear()
	$Tiles/Corners.clear()
	var floor_tiles = []
	for i in range(len(map.terrain)):
		for j in range(len(map.terrain[i])):
			if map.terrain[i][j] == MapEnums.TerrainType.WALL:
				navgrid.set_point_solid(Vector2i(j, i))
			else:
				floor_tiles.append(Vector2i(j, i))
			var tile: Vector2i = MapConstants.terrain_to_tileset_default[map.terrain[i][j]]
			var tile_variants: Array[Vector2i] = (
				MapConstants.terrain_to_tileset_variants[map.terrain[i][j]]
			)
			if randi_range(0,99) <= MapConstants.TILE_VARIANT_FREQUENCY:
				tile = tile_variants.pick_random()
			$Tiles/Terrain.set_cell(Vector2i(j, i), MapConstants.TILESET_SOURCE, tile)
	$Tiles/Walls.set_cells_terrain_connect(floor_tiles, 0, 0)
	$Tiles/Corners.set_cells_terrain_connect(floor_tiles, 1, 0)

	Util.log_process("spawning player and exit", 1)
	var spawned_actor_locs = [map.exit_loc]
	_update_actor_loc($ExitSign, map.exit_loc)

	_spawn_at_random_location($Player, spawned_actor_locs)
	$Player/Camera2D.limit_right = len(map.terrain[0]) * Constants.SPRITE_SIZE
	$Player/Camera2D.limit_bottom = len(map.terrain) * Constants.SPRITE_SIZE

	# Create mobs and gold
	Util.log_process("spawning mobs", 1)
	var is_raging_ai: bool = MapEnums.LevelModifier.raging_ai in mission.modifiers
	var spawned_mobs: int = 0
	var spawned_gold: int = 0
	for room_idx in range(len(map.room_floors)):
		var mob_count = 0
		var gold_count = 0
		var count_rand = randi_range(1, 100)
		while (
			mob_count < len(mission.mob_frequencies) and
			mission.mob_frequencies[mob_count] < count_rand
		):
			mob_count += 1
		Util.log_rng(
			"room " + str(room_idx) + " mobspawn " + str(count_rand) + "->" + str(mob_count), 1
		)
		count_rand = randi_range(1, 100)
		while (
			gold_count < len(mission.gold_frequencies) and
			mission.gold_frequencies[gold_count] < count_rand
		):
			gold_count += 1
		Util.log_rng(
			"room " + str(room_idx) + " goldspawn " + str(count_rand) + "->" + str(gold_count), 1
		)

		for i in range(mob_count):
			_spawn_mob(spawned_actor_locs, room_idx, is_raging_ai)
			spawned_mobs += 1
		for i in range(gold_count):
			_spawn_gold(spawned_actor_locs, room_idx)
			spawned_gold += 1

	# Make sure to spawn enough mobs and gold to allow the mission to complete.
	Util.log_process("finish spawning mobs", 1)
	while spawned_mobs < mission.min_mob_count:
		_spawn_mob(spawned_actor_locs, -1, is_raging_ai)
		spawned_mobs += 1
	while spawned_gold < mission.min_gold_count:
		_spawn_gold(spawned_actor_locs)
		spawned_gold += 1
	# TODO: determine pickup count/types in mission data
	Util.log_process("spawning pickups", 1)
	for i in range(2):
		var pickup_type = (
			ActorEnums.PickupType.DAGGER if randf() > 0.5 else ActorEnums.PickupType.SMOKE_BOMB
		)
		Util.log_rng("pickupspawn " + ActorEnums.PickupType.find_key(pickup_type), 2)
		_spawn_pickup(spawned_actor_locs, pickup_type)

	$HUD.update_score(HighScores.current_run["score"])

	Util.log_process("updating indicators", 1)
	for mob_ in get_tree().get_nodes_in_group("mob"):
		if mob_.is_queued_for_deletion():
			continue
		mob_.update_indicators()

	$Player.update_indicators()


func _spawn_mob(spawned_actor_locs, room_idx: int = -1, is_raging_ai: bool = false):
	var mob: Mob = mob_scene.instantiate()
	mob.map = navgrid
	mob.raging_ai = is_raging_ai
	_spawn_at_random_location(mob, spawned_actor_locs, room_idx)
	mob.set_random_name()
	mob.connect("mouseover", _on_mouseover)
	mob.connect("mouseoff", _on_mouseoff)
	mob.connect("clicked", _on_actor_click)
	mob.build_patrol_path()
	add_child(mob)


func _spawn_gold(spawned_actor_locs, room_idx: int = -1):
	var gold: Pickup = gold_scene.instantiate()
	_spawn_at_random_location(gold, spawned_actor_locs, room_idx)
	gold.connect("mouseover", _on_mouseover)
	gold.connect("mouseoff", _on_mouseoff)
	add_child(gold)


func _spawn_pickup(spawned_actor_locs, pickup_type: ActorEnums.PickupType):
	var pickup: Pickup = gold_scene.instantiate()
	_spawn_at_random_location(pickup, spawned_actor_locs)
	pickup.connect("mouseover", _on_mouseover)
	pickup.connect("mouseoff", _on_mouseoff)
	add_child(pickup)
	# animation cannot be changed until node is ready, so we need to init pickup_type after adding
	pickup.pickup_type = pickup_type


func reset_camera():
	$Player/Camera2D.reset_smoothing()


func _update_actor_loc(actor: Actor, loc: Vector2i):
	_add_tween(actor.move_to(loc, $Tiles/Terrain.map_to_local(loc)))


## Short helper method so I can one-line the spots where I need to queue animations.
func _add_tween(tween: Tween):
	if tween != null:
		_running_animations.append(tween)


func _animations_playing() -> bool:
	if _running_animations.is_empty():
		return false

	var new_queue: Array[Tween] = []
	for tween in _running_animations:
		if tween.is_running():
			new_queue.append(tween)

	_running_animations = new_queue
	return not _running_animations.is_empty()


func _spawn_at_random_location(actor: Actor, existing_actor_locs: Array, room_idx: int = -1) -> bool:
	if room_idx < 0:
		room_idx = randi_range(0, len(map.room_floors) - 1)
		Util.log_rng("spawn room idx " + str(room_idx), 2)
	var room_floors: Array = map.room_floors[room_idx]
	var loc: Vector2i
	while true:
		loc = room_floors.pick_random()
		Util.log_rng("spawn loc attempt " + str(loc), 3)
		if (
			loc not in existing_actor_locs and
			map.terrain[loc.y][loc.x] == MapEnums.TerrainType.FLOOR
		):
			break
	Util.log_rng("spawn loc " + str(loc), 2)

	existing_actor_locs.append(loc)
	_update_actor_loc(actor, loc)
	if not actor.is_connected("died", _on_actor_death.bind(actor)):
		actor.connect("died", _on_actor_death.bind(actor))
	return true


func _process(_delta):
	if not is_processing_input():
		return

	if _indicators_need_updating and not _animations_playing():
		_indicators_need_updating = false
		for mob: Mob in get_tree().get_nodes_in_group("mob"):
			mob.update_ai()
		for actor: Actor in get_tree().get_nodes_in_group("actor"):
			actor.update_indicators()

	var player_move
	if queued_action != null:
		player_move = Vector2i.ZERO
	else:
		player_move = $Player.decide_next_move()
	if player_move == null or not $TurnDelayTimer.is_stopped():
		return
	if _animations_playing():
		return
	move_count += 1
	Util.log_tick(str(move_count), 1)
	# process the player first, so the mobs can more intelligently cut them off
	if $Player.invisible:
		$Player.decrement_invisible_timer()
	Util.log_player("move " + str(player_move), 2)
	_perform_action($Player, player_move)

	# now process everyone else
	var move_queue = []
	var mobs = get_tree().get_nodes_in_group("mob")
	for mob in mobs:
		if mob.is_queued_for_deletion() or mob.dead:
			continue
		# We add the actor and how many times we've tried to move them.
		move_queue.push_back([mob, 0])
		# Set all the mobs to solid, so mobs behind will try to move around them. This doesn't
		# provide ideal behavior, but it should be better than I was doing before.
		navgrid.set_point_solid(mob.location)

	var blocked_actors = []
	while len(move_queue) > 0:
		var next_actor = move_queue.pop_front()
		var actor = next_actor[0]
		if actor.dead or actor.is_queued_for_deletion():
			continue
		var old_loc = actor.location
		var attempt_count = next_actor[1] + 1
		var desired_move = actor.decide_next_move()
		Util.log_ai(str(actor) + " move " + str(desired_move), 2)
		# Only try to move mobs which actually want to move. This is to prevent an issue with
		# multiple tweens being queued up which results in incorrect sprite rotations.
		var move_successful = false
		if desired_move != Vector2i.ZERO:
			move_successful = _perform_action(actor, desired_move)
		if move_successful:
			navgrid.set_point_solid(old_loc, false)
			navgrid.set_point_solid(actor.location)
		elif attempt_count < MAX_MOVE_ATTEMPTS:
			Util.log_ai(str(actor) + " blocked", 1)
			move_queue.push_back([actor, attempt_count])
		else:
			blocked_actors.push_back(actor)

	# Now try again for all the ones which have failed so far, but without solid mobs.
	for mob in mobs:
		navgrid.set_point_solid(mob.location, false)
	while len(blocked_actors) > 0:
		var actor = blocked_actors.pop_front()
		var desired_move = actor.decide_next_move()
		Util.log_ai(str(actor) + " last attempt " + str(desired_move), 1)
		var move_successful = _perform_action(actor, desired_move)

		# If the mob is still blocked, have them select a new patrol route.
		if not move_successful:
			actor.build_patrol_path()

	_indicators_need_updating = true
	for actor: Actor in get_tree().get_nodes_in_group("actor"):
		actor.update_indicators()
	$TurnDelayTimer.start()


## Attempts to move the actor in the desired direction.
## If the actor successfully moves or attacks, returns true. Otherwise, false.
func _perform_action(actor: Actor, direction: Vector2i) -> bool:
	actor.update_facing(direction)
	var action: Action
	if actor == $Player and queued_action != null:
		action = queued_action
		queued_action = null
	else:
		action = _determine_move_result(actor, direction)

	var log_type: Enums.LogType = Enums.LogType.PLAYER
	var additional_text: String = ""
	var log_level: int = 1
	if actor != $Player:
		log_type = Enums.LogType.AI
		additional_text = str(actor) + " "
		if action.result in [ActionResult.MOVE, ActionResult.STAY]:
			log_level = 2
	Util.log(
		additional_text + "action " + ActionResult.find_key(action.result) + " "
		+ str(action.actee) + " " + str(action.additional_info),
		log_type,
		log_level,
	)

	if action.result == ActionResult.STAY:
		_add_tween(actor.bounce_in_place())
		return false
	elif action.result == ActionResult.MOVE:
		_update_actor_loc(actor, actor.location + direction)
		actor.play_footstep_sounds()
	elif action.result == ActionResult.ATTACK:
		_add_tween(actor.attack_at(action.actee.location))
		action.actee.receive_damage(actor, actor.damage)
	elif action.result == ActionResult.MOVE_AND_PICKUP:
		_update_actor_loc(actor, actor.location + direction)
		actor.add_to_inventory(action.additional_info[0])
		action.actee.mark_dead()
	elif action.result == ActionResult.EXIT:
		mission.update_progress(Mission.ActivityType.exit_reached)
	elif action.result == ActionResult.USE_ITEM:
		var item_type: ActorEnums.PickupType = action.additional_info[0]
		if item_type == ActorEnums.PickupType.DAGGER and action.actee.is_in_group("mob"):
			actor.use_item(item_type)
			# TODO: maybe have dagger do a different amount of damage?
			action.actee.receive_damage(actor, actor.damage)
		elif item_type == ActorEnums.PickupType.SMOKE_BOMB and action.actee == actor:
			actor.use_item(item_type)
			_use_smoke_bomb(actor)
	return true


func _determine_move_result(actor: Actor, direction: Vector2i) -> Action:
	var cur_loc = actor.location
	var desired_loc = cur_loc + direction
	if map.terrain[desired_loc.y][desired_loc.x] == MapEnums.TerrainType.WALL:
		return Action.new(ActionResult.STAY, null)
	elif direction == Vector2i.ZERO:
		return Action.new(ActionResult.STAY, null)

	var action: Action = Action.new(ActionResult.MOVE, null)
	for other_actor in get_tree().get_nodes_in_group("actor"):
		if other_actor == actor:
			continue
		if desired_loc == other_actor.location and not other_actor.dead:
			# TODO: handle faction alignment instead of just Player vs non-Player
			if (
				(actor == $Player and other_actor.is_in_group("mob")) or
				(other_actor == $Player and actor.is_in_group("mob"))
			):
				action = Action.new(ActionResult.ATTACK, other_actor)
			elif actor == $Player and other_actor.is_in_group("pickup"):
				# Prioritize killing enemies over picking up loot.
				if action.result != ActionResult.MOVE:
					continue
				# Only pickup inventory items if the player has room.
				if other_actor.is_in_group("gold") or $Player.has_inventory_space():
					action = Action.new(
						ActionResult.MOVE_AND_PICKUP, other_actor, [other_actor.pickup_type]
					)
				else:
					action = Action.new(ActionResult.MOVE, null)
			elif actor == $Player and other_actor == $ExitSign:
				action = Action.new(ActionResult.EXIT, null)
			elif actor.is_in_group("mob") and other_actor.is_in_group("mob"):
				action = Action.new(ActionResult.STAY, null)
	return action


func _on_actor_death(actor: Actor):
	if actor is Player:
		HighScores.reset_score()
		player_died.emit()
	else:
		actor.ready_for_queue_free()
		HighScores.update_score(actor.score_value)
		$HUD.update_score(HighScores.current_run["score"])
		score_updated.emit()
		if actor.is_in_group("mob"):
			stats["Mobs Killed"] += 1
			mission.update_progress(Mission.ActivityType.mob_killed)
		elif actor.is_in_group("gold"):
			stats["Gold Looted"] += 1
			mission.update_progress(Mission.ActivityType.gold_looted)


func _use_smoke_bomb(actor: Actor):
	actor.invisible = true
	for mob: Mob in get_tree().get_nodes_in_group("mob"):
		mob.forget_player()


func _toggle_fog_of_war():
	$Player/Lights.visible = GameSettings.get_setting_by_name("fog_of_war")
	$CanvasModulate.visible = GameSettings.get_setting_by_name("fog_of_war")


func _toggle_grid_lines():
	$DebugLines.visible = GameSettings.get_setting_by_name("show_grid_lines")


func _on_player_damaged() -> void:
	$HUD.update_player_hp($Player.cur_hp, $Player.max_hp)


func _on_mouseover(info: TooltipInfo) -> void:
	$Tooltip.show()
	$Tooltip.set_info(info)


func _on_mouseoff() -> void:
	$Tooltip.hide()


func _on_actor_click(actor: Actor) -> void:
	if actor == $Player and $Player.has_item(ActorEnums.PickupType.SMOKE_BOMB):
		queued_action = Action.new(ActionResult.USE_ITEM, actor, [ActorEnums.PickupType.SMOKE_BOMB])
	elif actor.is_in_group("mob") and $Player.has_item(ActorEnums.PickupType.DAGGER):
		var visibility_ray = RayCast2D.new()
		add_child(visibility_ray)
		visibility_ray.position = actor.position
		# "target_position" really should be renamed "target vector" :facepalm:
		visibility_ray.target_position = $Player.position - actor.position
		visibility_ray.force_raycast_update()
		if visibility_ray.is_colliding():
			visibility_ray.queue_free()
			return
		visibility_ray.queue_free()
		queued_action = Action.new(ActionResult.USE_ITEM, actor, [ActorEnums.PickupType.DAGGER])


func _on_goal_complete(score: int) -> void:
	HighScores.update_score(score)
	$HUD.update_score(HighScores.current_run["score"])
	score_updated.emit()
	$HUD.update_goals(mission.get_current_goals(), mission.get_optional_goals())


func _on_stage_complete(is_last: bool) -> void:
	$HUD.update_goals(mission.get_current_goals(), mission.get_optional_goals())
	if is_last:
		level_complete.emit(stats)
