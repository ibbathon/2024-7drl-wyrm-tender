extends Node
class_name Savable

## The filename to save data to.
var filename: String

## Method which returns the savable data as a dictionary.
## Must be overridden to provide the data that this savable should save to file.
func _savable_data() -> Dictionary:
	return {}


## Takes data loaded from file and loads it into the node.
## Should return true if the data is valid, false otherwise.
func _load_data(_data: Dictionary) -> bool:
	return false


## Saves savable data to a file.
## Returns true if successful, false otherwise.
func save_to_file() -> bool:
	var file = FileAccess.open("user://" + filename, FileAccess.WRITE)
	var json_data = _savable_data()
	var json_string = JSON.stringify(json_data)
	file.store_string(json_string)
	file.close()
	return true


## Loads savable data from a file.
## Returns true if successful, false otherwise.
func load_from_file() -> bool:
	var json_data = Util.load_json_file("user://" + filename)
	if "error" in json_data:
		return false
	var successful = _load_data(json_data)
	return successful
