extends Node

var dragon_name: String
var level_active: bool = false
var prev_scale: float = 1.0
var is_paused: bool = true


func _ready():
	dragon_name = KoboldNames.gen_dragon_name()
	# start paused
	_pause_game()

	# connections
	MapGen.connect("progress_update", _on_map_gen_progress_update)
	GameSettings.settings_changed.connect(_settings_changed)
	_settings_changed()


func _on_map_gen_progress_update(step: String, pcnt: float):
	$UI/ProgressMeter.set_progress(step, pcnt)


func _input(event: InputEvent) -> void:
	if event.is_action_released("ui_cancel"):
		if not is_paused:
			get_viewport().set_input_as_handled()
			_pause_game()
		# Only allow unpausing if there's an active level and we're not in a sub-menu
		elif $UI/MainMenu.visible and $UI/MainMenu.in_base_menu() and level_active:
			get_viewport().set_input_as_handled()
			_resume_game()


func _on_player_death():
	$UI/MainMenu/TopLevelMenu/HighScoresUI.update_scores()
	$UI/GameOver.visible = true
	level_active = false
	_pause_game()
	$Music.stop()


func _on_level_score_updated():
	$UI/MainMenu/TopLevelMenu/HighScoresUI.update_scores()


func _generate_cave_system(map_settings):
	MapGen.build_cave_system(map_settings)


func get_random_mission(mission_type_str: String) -> Mission:
	var mission_type: MissionType
	if mission_type_str == "random":
		var mission_rand: int = randi_range(0, Data.mission_frequency_list[-1][0])
		for mission_freq in Data.mission_frequency_list:
			if mission_rand <= mission_freq[0]:
				mission_type = Data.mission_types[mission_freq[1]]
				break
		Util.log_rng(mission_type.filename, 1)
	else:
		mission_type = Data.mission_types[mission_type_str]
	var mission: Mission = mission_type.generate_mission()
	return mission


func _on_main_menu_new_game():
	level_active = false
	$Music.stop()
	HighScores.reset_score($UI/MainMenu.new_game_settings["name"], dragon_name)
	$UI/MainMenu/TopLevelMenu/HighScoresUI.update_scores()
	$UI/GameOver.visible = false
	$UI/MainMenu.visible = false

	var mission: Mission = get_random_mission($UI/MainMenu.new_game_settings["mission type"])
	var map_settings = $UI/MainMenu.new_game_settings["map settings"]
	if not $UI/MainMenu.new_game_settings["override maptype"]:
		map_settings.map_type = mission.map_type
	if not $UI/MainMenu.new_game_settings["override mapgen"]:
		map_settings.room_frequency = mission.room_freq
		map_settings.room_size = mission.room_size
		map_settings.cave_size = mission.cave_size

	await create_new_level($UI/MainMenu.new_game_settings, mission, true)

	$Level.set_player_name($UI/MainMenu.new_game_settings["name"])
	$UI/IntroScreen.update_name($UI/MainMenu.new_game_settings["name"])
	$UI/IntroScreen.update_intro_text(
		mission.mission_text,
		dragon_name,
		mission.mob_types[0].plural_name,
	)
	$UI/IntroScreen.visible = true
	$StartGameSound.play()
	await $UI/IntroScreen.continue_game
	$StartGameSound.stop()
	$UI/IntroScreen.visible = false
	reveal_level()
	_resume_game()
	$Music.play()
	level_active = true


func create_new_level(settings, mission: Mission, new_player: bool = false):
	Util.log_player(
		"Starting new level with settings: " + str($UI/MainMenu.new_game_settings)
		+ " and new_player: " + str(new_player),
		1,
	)
	$UI/ProgressMeter.visible = true
	hide_level()

	var reset_thread = Thread.new()
	reset_thread.start(_generate_cave_system.bind(settings["map settings"]))
	# reset_thread.wait_to_finish will block the UI updates, so use a signal instead
	var map: Map = await MapGen.cave_generated
	reset_thread.wait_to_finish() # this is purely so the editor doesn't complain about orphan threads

	$Level.map = map
	$Level.mission = mission
	$Level.reset(new_player)
	$UI/ProgressMeter.visible = false


func _on_main_menu_quit_game():
	get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	get_tree().quit()


func _on_main_menu_resume_game():
	_resume_game()


func _on_level_level_complete(stats: Dictionary) -> void:
	level_active = false
	HighScores.update_score(0, 1)
	_pause_game(false)

	$UI/LevelCompleteScreen.show()
	$UI/LevelCompleteScreen.update_stats(stats)
	$Music.stream_paused = true
	$VictorySound.play()
	await $UI/LevelCompleteScreen.continue_game
	$VictorySound.stop()
	$Music.stream_paused = false
	$UI/LevelCompleteScreen.hide()

	var mission: Mission = get_random_mission($UI/MainMenu.new_game_settings["mission type"])
	if not $UI/MainMenu.new_game_settings["override mapgen"]:
		var map_settings = $UI/MainMenu.new_game_settings["map settings"]
		map_settings.map_type = mission.map_type
		map_settings.room_frequency = mission.room_freq
		map_settings.room_size = mission.room_size
		map_settings.cave_size = mission.cave_size

	await create_new_level($UI/MainMenu.new_game_settings, mission)

	$UI/IntroScreen.update_intro_text(
		mission.mission_text,
		dragon_name,
		mission.mob_types[0].plural_name,
	)
	$UI/IntroScreen.visible = true
	$StartGameSound.play()
	await $UI/IntroScreen.continue_game
	$StartGameSound.stop()
	$UI/IntroScreen.visible = false
	reveal_level()
	_resume_game()
	$Music.play()
	level_active = true


func hide_level():
	$Level.hide()
	$Level/HUD.hide()


func reveal_level():
	$Level.show()
	$Level/HUD.show()
	$Level.reset_camera()


## A helper function to pause the level/game. I have futzed with the particular of this so many
## times that it just makes sense to make it a function. See _resume_game for the reverse.
func _pause_game(show_menu: bool = true) -> void:
	if show_menu:
		$UI/MainMenu.show_menu(level_active)
	#get_tree().paused = true
	$Level.set_process_input(false)
	is_paused = true


func _resume_game() -> void:
	$UI/MainMenu.hide()
	#get_tree().paused = false
	$Level.set_process_input(true)
	is_paused = false


func _settings_changed():
	# adjust window size for high-res monitors
	var new_scale = float(GameSettings.get_setting_by_name("window_scale"))
	var old_window_size = Vector2i(Vector2i.ONE * 11 * Constants.SPRITE_SIZE * prev_scale)
	var new_window_size = Vector2i(Vector2i.ONE * 11 * Constants.SPRITE_SIZE * new_scale)
	var cur_window_size = get_window().size
	get_window().content_scale_factor = new_scale
	# only resize the window if the user has not manipulated its size, or the window is too small
	# but never resize if the window is maximized/fullscreen (causes weird issues on Windows)
	if (
		get_window().mode == Window.Mode.MODE_WINDOWED and (
			cur_window_size == old_window_size
			or cur_window_size.x < new_window_size.x
			or cur_window_size.y < new_window_size.y
		)
	):
		get_window().size = new_window_size
	prev_scale = new_scale

	# adjust volume levels
	var bus = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(bus, linear_to_db(
		GameSettings.settings["Volume"]["master_volume"] / 100.0
	))
	bus = AudioServer.get_bus_index("Music")
	AudioServer.set_bus_volume_db(bus, linear_to_db(
		GameSettings.settings["Volume"]["music_volume"] / 100.0
	))
	bus = AudioServer.get_bus_index("Sfx")
	AudioServer.set_bus_volume_db(bus, linear_to_db(
		GameSettings.settings["Volume"]["sfx_volume"] / 100.0
	))
