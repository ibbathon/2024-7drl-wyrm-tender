extends Savable
var Setting = preload("res://setting.gd")
var SettingType = Setting.SettingType

signal settings_changed

const VERSION = 5
## full_settings is only meant to be accessed by the UI code, to build the settings pages.
func log_level_setting(name_: String, desc: String):
	return Setting.new(
		SettingType.SLIDER, name_, desc, 1, Setting.SliderSubsettings.new(0, 3, 1)
	)

var full_settings: Dictionary = {
	"Volume": [
		Setting.new(SettingType.PCNT, "master_volume", "Master Volume", 100),
		Setting.new(SettingType.PCNT, "music_volume", "Music Volume", 100),
		Setting.new(SettingType.PCNT, "sfx_volume", "Sfx Volume", 100),
	],
	"Video": [
		Setting.new(
			SettingType.DROPDOWN, "window_scale", "Window Scaling", "1.0",
			Setting.DropdownSubsettings.new({
				"1x": "1.0",
				"1.5x": "1.5",
				"2x": "2.0",
			}),
		),
		Setting.new(SettingType.HIDDEN, "show_ai_lines", "Show AI Lines", false),
		Setting.new(SettingType.HIDDEN, "show_grid_lines", "Show Grid Lines", false),
		Setting.new(SettingType.HIDDEN, "allow_zoom", "Allow Zoom", false),
		Setting.new(SettingType.HIDDEN, "fog_of_war", "Fog of War", true),
	],
	"Logging": [
		log_level_setting("player", "Player Log Level"),
		log_level_setting("ai", "AI Log Level"),
		log_level_setting("rng", "RNG Log Level"),
		log_level_setting("tick", "Turn Tick Log Level"),
		log_level_setting("process", "Process Log Level"),
		log_level_setting("pathing", "Pathing Log Level"),
	],
}
var _settings: Dictionary
## settings is a memoized setgetter wrapped around the full_settings which returns a
## dict-of-dicts-of-values with first-tier keys being the tabs and second-tier keys being the
## setting names. This makes it easier for other code to look up their necessary values, and also
## makes loading/saving the settings as simple as just a get or set.
var settings: Dictionary:
	get: return _settings
	set(new_settings):
		for tab_name in full_settings:
			for setting in full_settings[tab_name]:
				if tab_name in new_settings and setting.name in new_settings[tab_name]:
					setting.value = new_settings[tab_name][setting.name]
		settings_changed.emit()

var _settings_by_name: Dictionary


func _init():
	filename = "settings.save"


func _ready():
	# load persisted settings automatically on start
	settings_changed.connect(_memoize_settings)
	if not load_from_file():
		# if the file failed to load, we still need to memoize and then default the settings
		_memoize_settings()
		save_to_file()
	settings_changed.connect(save_to_file)

	for tabname in settings:
		if tabname == "version":
			continue
		for setting in settings[tabname]:
			if setting in _settings_by_name:
				push_warning(
					"%s is duplicated: %s and %s" % [
						setting,
						_settings_by_name[setting],
						[tabname, setting],
					]
				)
				continue
			_settings_by_name[setting] = [tabname, setting] as Array[String]


func _savable_data() -> Dictionary:
	return settings


func _load_data(data: Dictionary) -> bool:
	data = _convert_old_settings(data)
	settings = data
	return true


func _convert_old_settings(old_settings: Dictionary):
	# Version 1 to Version 2
	if "version" not in old_settings:
		var new_settings = {"version": 2, "Volume": {}, "Feature Flags": {}, "Debug": {}}
		for key in old_settings:
			if key in ["master_volume", "music_volume", "sfx_volume"]:
				new_settings["Volume"][key] = old_settings[key]
			elif key in ["use_new_ai", "use_new_path_gen"]:
				new_settings["Feature Flags"][key] = old_settings[key]
			elif key in ["show_ai_lines", "fog_of_war", "show_grid_lines"]:
				new_settings["Debug"][key] = old_settings[key]
		old_settings = new_settings
	# Version 2 to Version 3
	if old_settings["version"] < 3:
		old_settings["version"] = 3
		old_settings.get_or_add("Feature Flags", {})
		# Force the new features on for anyone who doesn't already have them on.
		old_settings["Feature Flags"]["use_new_ai"] = true
		old_settings["Feature Flags"]["use_new_path_gen"] = true
	# Version 3 to Version 4
	if old_settings["version"] < 4:
		old_settings["version"] = 4
		old_settings.get_or_add("Feature Flags", {})
		old_settings.get_or_add("Debug", {})
		# Move fog_of_war to Feature Flags and force it on.
		old_settings["Debug"].erase("fog_of_war")
		old_settings["Feature Flags"]["fog_of_war"] = true
	if old_settings["version"] < 5:
		old_settings["version"] = 5
		var window_size = old_settings.get_or_add("Video", {}).get("window_size", false)
		old_settings["Video"]["window_scale"] = "2.0" if window_size else "1.0"

	return old_settings


func _memoize_settings():
	_settings = {"version": VERSION}
	for tab_name in full_settings:
		_settings[tab_name] = {}
		for setting in full_settings[tab_name]:
			if setting.name in _settings[tab_name]:
				push_warning(
					"%s is duplicated in same tab: %s" % [
						setting.name,
						[tab_name, setting.name],
					]
				)
				continue
			_settings[tab_name][setting.name] = setting.value


func get_setting_by_name(setting_name: String) -> Variant:
	if setting_name not in _settings_by_name:
		return null
	var setting_keys: Array[String] = _settings_by_name[setting_name]
	return settings[setting_keys[0]][setting_keys[1]]
