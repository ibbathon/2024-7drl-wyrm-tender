extends Node
class_name Enums

enum LogType {
	PLAYER,
	AI,
	RNG,
	TICK,
	PROCESS,
	PATHING,
}
