extends Node

var log_type_to_setting: Dictionary = {
	Enums.LogType.PLAYER: "player",
	Enums.LogType.AI: "ai",
	Enums.LogType.RNG: "rng",
	Enums.LogType.TICK: "tick",
	Enums.LogType.PROCESS: "process",
	Enums.LogType.PATHING: "pathing",
}


func log_error(text: String) -> void:
	push_error(text)
	print(text)


func log(text: String, log_type: Enums.LogType, verbosity: int):
	if GameSettings.get_setting_by_name(log_type_to_setting[log_type]) >= verbosity:
		print(Enums.LogType.find_key(log_type), ": ", text)

# helper methods so not every line is super-long
func log_player(text: String, verbosity: int): self.log(text, Enums.LogType.PLAYER, verbosity)
func log_ai(text: String, verbosity: int): self.log(text, Enums.LogType.AI, verbosity)
func log_rng(text: String, verbosity: int): self.log(text, Enums.LogType.RNG, verbosity)
func log_tick(text: String, verbosity: int): self.log(text, Enums.LogType.TICK, verbosity)
func log_process(text: String, verbosity: int): self.log(text, Enums.LogType.PROCESS, verbosity)
func log_pathing(text: String, verbosity: int): self.log(text, Enums.LogType.PATHING, verbosity)


## Loads and parses a JSON file, returning the result or an error message if the load failed.
## If the load fails, logs an error with the results.
func load_json_file(filename: String) -> Dictionary:
	if not FileAccess.file_exists(filename):
		var error = "File " + filename + " does not exist"
		log_error(error)
		return {"error": error}

	var data_raw = FileAccess.get_file_as_string(filename)
	var json = JSON.new()

	var parse_result = json.parse(data_raw)
	if not parse_result == OK:
		var error = (
			"File " + filename + " parse error: " + json.get_error_message() +
			" on line " + str(json.get_error_line())
		)
		log_error(error)
		return {"error": error}

	return json.get_data()
