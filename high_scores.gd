extends Savable

var scores: Array = [
	{"kobold": "Ibb", "dragon": "Solas", "levels": 1, "score": 10},
	{"kobold": "Richard", "dragon": "Solas", "levels": 0, "score": 5},
	{"kobold": "Sqwerl", "dragon": "Narf", "levels": 0, "score": 2},
	{"kobold": "Wander", "dragon": "Ibb", "levels": 0, "score": 0},
	{"kobold": "Sassy", "dragon": "Squish", "levels": 0, "score": 0},
]
var last_run: Dictionary
var current_run: Dictionary


func _init():
	filename = "highscores.json"


func _ready():
	load_from_file()


func _notification(note):
	if note == NOTIFICATION_WM_CLOSE_REQUEST:
		reset_score("", "")
		save_to_file()


## Increments the current score by score_inc (and levels completed by level_inc).
func update_score(score_inc: int, level_inc: int = 0):
	if current_run == {}:
		push_error("Tried to update score without a current run")
		return
	current_run["score"] += score_inc
	current_run["levels"] += level_inc


## Resets the current run in prep for the next run (given by kobold and dragon).
func reset_score(kobold: String = "", dragon: String = ""):
	if current_run != {}:
		last_run = current_run
		_update_scores_table()
	if kobold != "" and dragon != "":
		current_run = {"kobold": kobold, "dragon": dragon, "levels": 0, "score": 0}
	else:
		current_run = {}


## Sorts two given scores in descending order by "score" and then "levels".
func _score_sort(a: Dictionary, b: Dictionary):
	if a["score"] != b["score"]:
		return a["score"] > b["score"]
	return a["levels"] > b["levels"]


## Adds the last run to the scores table, sorts it, and then discards down to 100.
func _update_scores_table():
	scores.append(last_run)
	scores.sort_custom(_score_sort)
	if len(scores) > 100:
		scores.pop_back()


## Returns the high score place for the last run, used on the high score UI.
## Returns -1 if no last run.
func get_last_run_place() -> int:
	var index = scores.find(last_run)
	if index >= 0:
		return index + 1
	if index == -1:
		return index
	return 101


func _savable_data() -> Dictionary:
	return {"scores": scores}


func _load_data(data: Dictionary) -> bool:
	if "scores" not in data:
		return false
	# TODO: probably need more data validation...
	scores = data["scores"]
	return true
